-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 13, 2018 at 07:13 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `orderlist`
--

CREATE TABLE `orderlist` (
  `id` int(11) NOT NULL,
  `ordernumber` bigint(20) NOT NULL,
  `productcode` varchar(90) NOT NULL,
  `name` varchar(90) NOT NULL,
  `sellingprice` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` double DEFAULT NULL,
  `totalcost` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderlist`
--

INSERT INTO `orderlist` (`id`, `ordernumber`, `productcode`, `name`, `sellingprice`, `quantity`, `total`, `totalcost`) VALUES
(7, 1533548502719, 'Elianto51', 'Elianto', 520, 4, 2080, NULL),
(8, 1533548502719, 'Soko4', 'Soko', 110, 1, 110, NULL),
(9, 1533548502719, 'Soko4', 'Soko', 110, 3, 330, NULL),
(15, 1533634751327, 'Soko4', 'Soko', 110, 6, 660, NULL),
(16, 1533634751327, 'Oliveb1', 'Olive oil', 975, 2, 1950, NULL),
(17, 1533634751327, 'Oliveb1', 'Olive oil', 975, 6, 5850, NULL),
(18, 1533634751327, 'Soko3', 'Soko', 110, 2, 220, NULL),
(19, 1533634751327, 'Persil2a', 'Persil', 160, 3, 480, NULL),
(20, 1533634751327, 'Persil2a', 'Persil', 160, 1, 160, NULL),
(34, 1533727511620, 'Oliveb1', 'Olive oil', 975, 2, 1950, NULL),
(35, 1533727511620, 'Omo1', 'Omo', 230, 2, 460, NULL),
(36, 1533727511620, 'Persil2a', 'Persil', 160, 1, 160, NULL),
(37, 1533727511620, 'Soko3', 'Soko', 110, 2, 220, NULL),
(39, 1533728002396, 'Soko4', 'Soko', 110, 1, 110, NULL),
(40, 1533730995899, 'Steelwire2', 'Steelwire', 15, 3, 45, NULL),
(41, 1533731020136, 'Persil2a', 'Persil', 160, 4, 640, NULL),
(42, 1533731207229, 'Oliveb1', 'Olive oil', 975, 2, 1950, NULL),
(43, 1533731228597, 'Persil2a', 'Persil', 160, 2, 320, NULL),
(45, 1534155019518, 'Soko3', 'Soko', 110, 2, 220, NULL),
(46, 1534155019518, 'Pampersb3', 'Pampers', 850, 3, 2550, NULL),
(48, 1534155205722, 'Soko4', 'Soko', 110, 1, 110, NULL),
(49, 1534155205722, 'Steelwire2', 'Steelwire', 15, 1, 15, NULL),
(50, 1534155205722, 'Jogoo1', 'Jogoo', 115, 1, 115, NULL),
(53, 1534156099909, 'Jogoo1', 'Jogoo', 115, 1, 115, NULL),
(54, 1534156099909, 'Pampersb3', 'Pampers', 850, 2, 1700, NULL),
(57, 1534156213902, 'Omo1', 'Omo', 230, 3, 690, NULL),
(58, 1534156213902, 'Oliveb1', 'Olive oil', 975, 1, 975, NULL),
(69, 1534159275831, 'Oliveb1', 'Olive oil', 975, 1, 975, NULL),
(70, 1534159275831, 'Pampersb3', 'Pampers', 850, 3, 2550, NULL),
(71, 1534159315777, 'Soko3', 'Soko', 110, 2, 220, NULL),
(72, 1534159315777, 'Steelwire2', 'Steelwire', 15, 2, 30, NULL),
(73, 1534159315777, 'Soko3', 'Soko', 110, 1, 110, NULL),
(74, 1534159315777, 'Soko3', 'Soko', 110, 4, 440, NULL),
(75, 1534159315777, 'Soko3', 'Soko', 110, 1, 110, NULL),
(76, 1534159315777, 'Soko3', 'Soko', 110, 4, 440, NULL),
(77, 1534159315777, 'Elianto51', 'Elianto', 520, 2, 1040, NULL),
(78, 1534159315777, 'Steelwire2', 'Steelwire', 15, 1, NULL, NULL),
(79, 1534159315777, 'Omo1', 'Omo', 3, 0, NULL, NULL),
(80, 1534159315777, 'Soko4', 'Soko', 0, 0, NULL, NULL),
(81, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(82, 1534159315777, 'Soko4', 'Soko', 2, 0, NULL, NULL),
(83, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(84, 1534159315777, 'Soko4', 'Soko', 2, 0, NULL, NULL),
(85, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(86, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(87, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(88, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(89, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(90, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(91, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(92, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(93, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(94, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(95, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(96, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(97, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL, NULL),
(98, 1534244746125, 'Soko4', 'Soko', 110, 1, NULL, NULL),
(99, 1534244931659, 'Oliveb1', 'Olive oil', 975, 1, NULL, NULL),
(100, 1534244931659, 'Oliveb1', 'Olive oil', 975, 1, NULL, NULL),
(101, 1534244931659, 'Pampersb3', 'Pampers', 850, 2, NULL, NULL),
(102, 1534244931659, 'Pampersb3', 'Pampers', 850, 4, NULL, NULL),
(103, 1534246438084, 'Oliveb1', 'Olive oil', 975, 1, 975, NULL),
(104, 1534246438084, 'Soko3', 'Soko', 110, 3, 330, NULL),
(105, 1534324680617, 'Steelwire2', 'Steelwire', 15, 1, 15, NULL),
(106, 1534324705254, 'Omo1', 'Omo', 230, 1, 230, NULL),
(107, 1534324705254, 'Steelwire2', 'Steelwire', 15, 1, 15, NULL),
(108, 1534325511619, 'Omo1', 'Omo', 230, 1, 230, NULL),
(109, 1534325511619, 'Omo1', 'Omo', 230, 2, 460, NULL),
(110, 1534325511619, 'Omo1', 'Omo', 230, 2, 460, NULL),
(111, 1534326250732, 'Omo1', 'Omo', 230, 3, 690, NULL),
(112, 1534326250732, 'Omo1', 'Omo', 230, 1, 230, NULL),
(113, 1534326250732, 'Omo1', 'Omo', 230, 3, 690, NULL),
(114, 1534326250732, 'Omo1', 'Omo', 230, 2, 460, NULL),
(115, 1534326250732, 'Omo1', 'Omo', 230, 1, 230, NULL),
(116, 1534326250732, 'Omo1', 'Omo', 230, 2, 460, NULL),
(117, 1534326250732, 'Omo1', 'Omo', 230, 1, 230, NULL),
(118, 1534328695110, 'Omo1', 'Omo', 230, 2, 460, NULL),
(119, 1534328695110, 'Omo1', 'Omo', 230, 1, 230, NULL),
(120, 1534328695110, 'Omo1', 'Omo', 230, 3, 690, NULL),
(121, 1534328695110, 'Omo1', 'Omo', 230, 3, 690, NULL),
(122, 1534328695110, 'Omo1', 'Omo', 230, 1, 230, NULL),
(123, 1534328695110, 'Omo1', 'Omo', 230, 1, 230, NULL),
(124, 1534328695110, 'Omo1', 'Omo', 230, 1, 230, NULL),
(125, 1534328695110, 'Omo1', 'Omo', 230, 2, 460, NULL),
(126, 1534328695110, 'Omo1', 'Omo', 230, 3, 690, NULL),
(127, 1534333165285, 'Omo1', 'Omo', 230, 2, 460, NULL),
(128, 1534333256854, 'Jogoo2', 'Jogoo', 115, 2, 230, NULL),
(129, 1534333256854, 'Omo1', 'Omo', 230, 1, 230, NULL),
(130, 1534333256854, 'Omo1', 'Omo', 230, 3, 690, NULL),
(131, 1534333256854, 'Omo1', 'Omo', 230, 2, 460, NULL),
(132, 1534334915808, 'Jogoo1', 'Jogoo', 115, 2, 230, NULL),
(133, 1534334915808, 'Oliveb1', 'Olive oil', 975, 1, 975, NULL),
(134, 1534334915808, 'Pampersb3', 'Pampers', 850, 3, 2550, NULL),
(135, 1535974252706, 'Omo1', 'Omo', 130, 1, 130, NULL),
(136, 1535975881615, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(137, 1535990216257, 'Elianto1', 'Elianto', 600, 7, 4200, NULL),
(138, 1535990216257, 'Elianto1', 'Elianto', 600, 8, 4800, NULL),
(139, 1535990216257, 'Elianto1', 'Elianto', 600, 8, 4800, NULL),
(140, 1536047261188, 'Omo1', 'Omo', 130, 1, 130, NULL),
(141, 1536047261188, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(142, 1536047261188, 'persil2', 'Persil', 160, 2, 320, NULL),
(143, 1536047261188, 'persil2', 'Persil', 160, 2, 320, NULL),
(144, 1536047261188, 'Omo1', 'Omo', 130, 2, 260, NULL),
(145, 1536054361268, 'persil2', 'Persil', 160, 3, 480, NULL),
(146, 1536054361268, 'persil2', 'Persil', 160, 2, 320, NULL),
(147, 1536054361268, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(148, 1536054361268, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(149, 1536054361268, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(150, 1536054361268, 'Omo1', 'Omo', 130, 1, 130, NULL),
(151, 1536158640390, 'Elianto', 'Elianto51', 230, 2, 460, NULL),
(152, 1536327150083, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(153, 1536327150083, 'Menengai2b', 'Menengai2', 130, 3, 390, NULL),
(154, 1536327756425, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(155, 1536327756425, 'Omo1', 'Omo', 130, 1, 130, NULL),
(156, 1536565648909, 'Elianto52', 'Elianto', 600, 2, 1200, NULL),
(160, 1536567758559, 'Omo1', 'Omo', 130, 1, 130, NULL),
(161, 1536567758559, 'pampersmid1', 'pampersmid', 160, 3, 480, NULL),
(162, 1536567945875, 'Menengai2b', 'Menengai2', 130, 0, 0, NULL),
(163, 1536567945875, 'Omo1', 'Omo', 130, 3, 390, NULL),
(164, 1536568000353, 'pampersmid1', 'pampersmid', 160, 0, 0, NULL),
(165, 1536568449108, 'persil2', 'Persil', 160, 0, 0, NULL),
(166, 1536568449108, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(167, 1536571779840, 'persil2', 'Persil', 160, 0, 0, NULL),
(168, 1536571779840, 'Menengai2b', 'Menengai2', 130, 1, 130, NULL),
(169, 1536571779840, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(170, 1536571779840, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(171, 1536571779840, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(172, 1536571779840, 'Omo1', 'Omo', 130, 2, 260, NULL),
(173, 1536572977564, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(174, 1536573126107, 'pampersmid1', 'pampersmid', 160, 0, 0, NULL),
(175, 1536573126107, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(176, 1536573615696, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(177, 1536573658448, 'Omo1', 'Omo', 130, 0, 0, NULL),
(178, 1536573779480, 'Elianto51', 'Elianto', 600, 0, 0, NULL),
(179, 1536574210676, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(180, 1536574435242, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(181, 1536574435242, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(182, 1536574435242, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(183, 1536575356199, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(184, 1536575651939, 'Elianto52', 'Elianto', 600, 4, 2400, NULL),
(185, 1536575651939, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(186, 1536577568764, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(187, 1536578275767, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(188, 1536578396418, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(189, 1536578508183, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(190, 1536578547422, 'Elianto52', 'Elianto', 600, 0, 0, NULL),
(191, 1536578547422, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(192, 1536580720578, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(193, 1536580820259, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(194, 1536580820259, 'Elianto52', 'Elianto', 600, 2, 1200, NULL),
(195, 1536580820259, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(196, 1536581312447, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(197, 1536581312447, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(198, 1536581312447, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(199, 1536582131261, 'Elianto51', 'Elianto', 600, 0, 0, NULL),
(200, 1536582340242, 'Elianto51', 'Elianto', 600, 0, 0, NULL),
(201, 1536582442302, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(202, 1536582510429, 'Elianto1', 'Elianto', 600, 0, 0, NULL),
(203, 1536582561562, 'Elianto51', 'Elianto', 600, 0, 0, NULL),
(204, 1536582732911, 'Elianto1', 'Elianto', 600, 1, 0, NULL),
(205, 1536582834601, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(206, 1536582834601, 'Omo1', 'Omo', 130, 2, 260, NULL),
(207, 1536582856392, 'persil2', 'Persil', 160, 3, 480, NULL),
(208, 1536582856392, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(209, 1536582856392, 'Menengai2b', 'Menengai2', 130, 1, 130, NULL),
(210, 1536650968103, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(211, 1536650979583, 'Omo1', 'Omo', 130, 2, 260, NULL),
(212, 1536650979583, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(213, 1536659774531, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(214, 1536659774531, 'Omo1', 'Omo', 130, 2, 260, NULL),
(215, 1536660285552, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(216, 1536660285552, 'persil2', 'Persil', 160, 1, 160, NULL),
(217, 1536660740259, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(218, 1536660740259, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(219, 1536660740259, 'pampersmid1', 'pampersmid', 160, 2, 320, NULL),
(220, 1536661358737, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(221, 1536661358737, 'pampersmid1', 'pampersmid', 160, 3, 480, NULL),
(222, 1536661488767, 'Elianto52', 'Elianto', 600, 4, 2400, NULL),
(223, 1536661488767, 'Omo1', 'Omo', 130, 2, 260, NULL),
(224, 1536661488767, 'persil2', 'Persil', 160, 4, 640, NULL),
(225, 1536661864324, 'Elianto52', 'Elianto', 600, 3, 1800, NULL),
(226, 1536661875409, 'Elianto52', 'Elianto', 600, 5, 3000, NULL),
(227, 1536661875409, 'pampersmid1', 'pampersmid', 160, 4, 640, NULL),
(228, 1536662316655, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(229, 1536669812720, 'Elianto52', 'Elianto', 600, 2, 1200, NULL),
(230, 1536671458638, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(231, 1536671458638, 'Menengai2b', 'Menengai2', 130, 1, 130, NULL),
(232, 1536671727131, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(233, 1536740973741, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(234, 1536740973741, 'Omo1', 'Omo', 130, 1, 130, NULL),
(235, 1536761270172, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(236, 1536761280914, 'Omo1', 'Omo', 130, 3, 390, NULL),
(237, 1536761463064, 'Elianto52', 'Elianto', 600, 2, 1200, NULL),
(238, 1536761475474, 'Omo1', 'Omo', 130, 3, 390, NULL),
(239, 1536827270842, 'Menengai2b', 'Menengai2', 130, 4, 520, NULL),
(240, 1536827468632, 'Menengai2b', 'Menengai2', 130, 4, 520, NULL),
(241, 1536827501503, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(242, 1536827501503, 'persil2', 'Persil', 160, 2, 320, NULL),
(243, 1536827637133, 'pampersmid1', 'pampersmid', 160, 2, 320, NULL),
(244, 1536827637133, 'Elianto51', 'Elianto', 600, 3, 1800, NULL),
(245, 1536827703866, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(246, 1536827728508, 'Menengai2b', 'Menengai2', 130, 4, 520, NULL),
(247, 1536827728508, 'persil2', 'Persil', 160, 2, 320, NULL),
(248, 1536828304260, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(249, 1536828411645, 'Menengai2b', 'Menengai2', 130, 1, 130, NULL),
(250, 1536828564955, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(251, 1536828623400, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(252, 1536828704345, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(253, 1536828819073, 'Menengai2b', 'Menengai2', 130, 4, 520, NULL),
(254, 1536854154040, 'Elianto1', 'Elianto', 600, 7, 4200, NULL),
(255, 1536854154040, 'Omo1', 'Omo', 130, 45, 5850, NULL),
(256, 1536912587762, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(257, 1536912587762, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(258, 1536912603459, 'Omo1', 'Omo', 130, 3, 390, NULL),
(259, 1536912603459, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(260, 1536919432339, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(261, 1536919432339, 'Omo1', 'Omo', 130, 1, 130, NULL),
(262, 1536927918423, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(263, 1536927966858, 'persil2', 'Persil', 160, 4, 640, NULL),
(264, 1537169539545, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(265, 1537169556616, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(266, 1537169556616, 'pampersmid1', 'pampersmid', 160, 3, 480, NULL),
(267, 1537169686014, 'Omo1', 'Omo', 130, 2, 260, NULL),
(268, 1537188443737, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(269, 1537345675390, 'Salt500g1', 'Salt500g', 20, 2, 40, NULL),
(270, 1537345815160, 'Salt500g1', 'Salt500g', 20, 5, 100, NULL),
(271, 1537362756799, 'Menengai2b', 'Menengai2', 130, 3, 390, NULL),
(272, 1537362756799, 'persil2', 'Persil', 160, 2, 320, NULL),
(273, 1537957424193, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(274, 1537957424193, 'Omo1', 'Omo', 130, 3, 390, NULL),
(275, 1537957445271, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(276, 1537957445271, 'Menengai2b', 'Menengai2', 130, 3, 390, NULL),
(277, 1537958241107, 'Elianto1', 'Elianto', 600, 4, 2400, NULL),
(278, 1537958241107, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(279, 1537958259637, 'persil2', 'Persil', 160, 1, 160, NULL),
(280, 1537961442204, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(281, 1537961469896, 'pampersmid1', 'pampersmid', 160, 4, 640, NULL),
(282, 1538031248984, 'pampersmid1', 'pampersmid', 160, 2, 320, NULL),
(283, 1538031248984, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(284, 1538032965746, 'Menengai2b', 'Menengai2', 130, 4, 520, NULL),
(285, 1538032965746, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(286, 1538382180586, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(287, 1538382180586, 'persil2', 'Persil', 160, 2, 320, NULL),
(288, 1538382198460, 'Omo1', 'Omo', 130, 1, 130, NULL),
(289, 1538382384608, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(290, 1538382384608, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(291, 1538382398848, 'persil2', 'Persil', 160, 1, 160, NULL),
(292, 1538382406590, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(293, 1538389048126, 'pampersmid1', 'pampersmid', 160, 2, 320, NULL),
(294, 1538390656913, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(295, 1538477138252, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(296, 1538478933599, 'Omo1', 'Omo', 130, 2, 260, NULL),
(297, 1538478945610, 'Omo1', 'Omo', 130, 1, 130, NULL),
(298, 1538478993994, 'Omo1', 'Omo', 130, 1, 130, NULL),
(299, 1538479004969, 'Omo1', 'Omo', 130, 4, 520, NULL),
(300, 1538551955281, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(301, 1538552018452, 'Omo1', 'Omo', 130, 1, 130, NULL),
(302, 1538553156766, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(303, 1538553156766, 'Elianto1', 'Elianto', 600, 3, 1800, NULL),
(304, 1538553562318, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(305, 1538553570420, 'Omo1', 'Omo', 130, 3, 390, NULL),
(306, 1538559726927, 'Menengai2b', 'Menengai2', 130, 3, 390, NULL),
(307, 1538559726927, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(308, 1538559860353, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(309, 1538559860353, 'persil2', 'Persil', 160, 1, 160, NULL),
(310, 1538559880601, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(311, 1538565831738, 'pampersmid1', 'pampersmid', 160, 1, 160, NULL),
(312, 1538566047904, 'Elianto1', 'Elianto', 600, 1, 600, NULL),
(313, 1538570858116, 'Elianto1', 'Elianto', 600, 2, 1200, NULL),
(314, 1538571158869, 'Omo1', 'Omo', 130, 2, 260, NULL),
(315, 1538571252725, 'Elianto51', 'Elianto', 600, 2, 1200, NULL),
(316, 1538571543724, 'persil2', 'Persil', 160, 2, 320, NULL),
(317, 1538571899740, 'Menengai2b', 'Menengai2', 130, 2, 260, NULL),
(318, 1538572066420, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(319, 1538572133895, 'Menengai2b', 'Menengai2', 130, 3, 390, NULL),
(320, 1538572519599, 'Elianto51', 'Elianto', 600, 1, 600, NULL),
(321, 1538572519599, 'persil2', 'Persil', 160, 1, 160, NULL),
(322, 1538573269983, 'Elianto1', 'Elianto', 600, 2, 1200, 0),
(323, 1538573609480, 'Elianto1', 'Elianto', 600, 1, 600, 0),
(324, 1538573609480, 'pampersmid1', 'pampersmid', 160, 2, 320, 0),
(325, 1538574329563, 'Omo1', 'Omo', 130, 3, 390, 0),
(326, 1538574345256, 'pampersmid1', 'pampersmid', 160, 4, 640, 0),
(327, 1538574743247, 'Elianto1', 'Elianto', 600, 2, 1200, 1100),
(328, 1538574743247, 'persil2', 'Persil', 160, 3, 480, 450),
(329, 1538574863404, 'Omo1', 'Omo', 130, 1, 130, 120),
(330, 1538574872945, 'persil2', 'Persil', 160, 3, 480, 450),
(331, 1538574872945, 'Elianto51', 'Elianto', 600, 1, 600, 550),
(332, 1538739821172, 'pampersmid1', 'pampersmid', 160, 1, 160, 150),
(333, 1538739821172, 'pampersmid1', 'pampersmid', 160, 1, 160, 150),
(334, 1538739907491, 'pampersmid1', 'pampersmid', 160, 2, 320, 300),
(335, 1538739943604, 'pampersmid1', 'pampersmid', 160, 1, 160, 150),
(336, 1538837924511, 'Elianto51', 'Elianto', 600, 2, 1200, 1100),
(337, 1538837924511, 'Omo2', 'Omo', 130, 2, 260, 240),
(338, 1538838029833, 'pampersmid1', 'pampersmid', 160, 3, 480, 450),
(339, 1539068693086, 'Elianto51', 'Elianto', 600, 2, 1200, 1100),
(340, 1539068693086, 'Omo2', 'Omo', 130, 1, 130, 120),
(341, 1539068709342, 'Menengai2b', 'Menengai2', 130, 3, 390, 360),
(342, 1539068709342, 'Salt500g2', 'Salt500g', 20, 1, 20, 15),
(343, 1539068746306, 'Oliveoil500g1', 'Oliveoil500g', 600, 1, 600, 540),
(344, 1539068746306, 'pampersmid1', 'pampersmid', 160, 1, 160, 150),
(345, 1539068770955, 'Salt500g1', 'Salt500g', 20, 1, 20, 15),
(346, 1539068779931, 'Elianto1', 'Elianto', 600, 2, 1200, 1100),
(347, 1539068779931, 'persil2', 'Persil', 160, 1, 160, 150),
(348, 1539078291474, 'Elianto1', 'Elianto', 600, 0, 0, 0),
(349, 1539078291474, 'Elianto1', 'Elianto', 600, 2, 1200, 1100),
(350, 1539178204907, 'Elianto51', 'Elianto', 600, 2, 1200, 1100),
(351, 1539178204907, 'Omo1', 'Omo', 130, 3, 390, 360),
(352, 1539179676168, 'Menengai2b', 'Menengai2', 130, 2, 260, 240),
(353, 1539179676168, 'pampersmid1', 'pampersmid', 160, 3, 480, 450),
(354, 1539179695053, 'Oliveoil500g1', 'Oliveoil500g', 600, 1, 600, 540),
(355, 1539179730727, 'pampersmid1', 'pampersmid', 160, 3, 480, 450),
(356, 1539179742150, 'Oliveoil500g1', 'Oliveoil500g', 600, 1, 600, 540),
(357, 1539332507286, 'Elianto1', 'Elianto', 600, 1, 600, 550),
(358, 1539332516090, 'Omo2', 'Omo', 130, 2, 260, 240),
(359, 1539332516090, 'pampersmid1', 'pampersmid', 160, 1, 160, 150),
(366, 1539337728471, 'Elianto1', 'Elianto', 600, 1, 600, 550),
(367, 1539337834242, 'Menengai2b', 'Menengai2', 130, 2, 260, 240),
(368, 1539337989035, 'Omo2', 'Omo', 130, 2, 260, 240),
(369, 1539338166991, 'Oliveoil500g1', 'Oliveoil500g', 600, 2, 1200, 1080),
(370, 1539338222383, 'Oliveoil500g1', 'Oliveoil500g', 600, 1, 600, 540),
(371, 1539338882319, 'Salt500g1', 'Salt500g', 20, 2, 40, 30),
(372, 1539339094843, 'Salt500g2', 'Salt500g', 20, 1, 20, 15),
(373, 1539339268259, 'Menengai2b', 'Menengai2', 130, 2, 260, 240),
(374, 1539339268259, 'Omo1', 'Omo', 130, 1, 130, 120),
(375, 1539339448642, 'Elianto1', 'Elianto', 600, 1, 600, 550),
(376, 1539339503978, 'pampersmid1', 'pampersmid', 160, 2, 320, 300),
(377, 1539339503978, 'Oliveoil500g1', 'Oliveoil500g', 600, 1, 600, 540),
(378, 1539340090675, 'Menengai2b', 'Menengai2', 130, 1, 130, 120),
(379, 1539340302763, 'Omo2', 'Omo', 130, 2, 260, 240),
(380, 1539340302763, 'Oliveoil500g1', 'Oliveoil500g', 600, 1, 600, 540),
(381, 1539340380771, 'Elianto51', 'Elianto', 600, 1, 600, 550);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_seller_id` int(11) NOT NULL,
  `order_status` varchar(90) NOT NULL,
  `totalsale` double DEFAULT NULL,
  `totalcost` double DEFAULT NULL,
  `endof_order` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_date`, `order_seller_id`, `order_status`, `totalsale`, `totalcost`, `endof_order`) VALUES
(1533548502719, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533634751327, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533718361338, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533727511620, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533728002396, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533730995899, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533731020136, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533731207229, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1533731228597, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534155019518, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534155205722, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534155913538, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534156099909, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534156213902, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534156213903, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534156213904, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534159275831, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534159315777, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534244746125, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534244931659, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534246438084, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534324680617, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534324705254, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534325511619, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534326250732, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534328695110, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534333165285, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534333256854, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1534334915808, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1535974252706, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1535975881615, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1535990216257, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1535991314907, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1536047261188, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1536054361268, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1536158640390, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1536327150083, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1536327756425, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1536565648909, '0000-00-00 00:00:00', 0, 'ongoing', NULL, NULL, NULL),
(1536567758559, '2018-09-09 21:00:00', 4, 'ongoing', NULL, NULL, NULL),
(1536567945875, '2018-09-09 21:00:00', 4, 'ongoing', NULL, NULL, NULL),
(1536568000353, '2018-09-09 21:00:00', 4, 'ongoing', NULL, NULL, NULL),
(1536568449108, '2018-09-09 21:00:00', 4, 'ongoing', NULL, NULL, NULL),
(1536571779840, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536572977564, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536573126107, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536573615696, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536573658448, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536573779480, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536574210676, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536574435242, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536575356199, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536575651939, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536577568764, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536578275767, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536578396418, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536578508183, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536578547422, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536580720578, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536580820259, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536581312447, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582131261, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582340242, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582442302, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582510429, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582561562, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582732911, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582834601, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536582856392, '2018-09-09 21:00:00', 7, 'ongoing', NULL, NULL, NULL),
(1536650968103, '2018-09-10 21:00:00', 4, 'ongoing', NULL, NULL, NULL),
(1536650979583, '2018-09-10 21:00:00', 4, 'ongoing', NULL, NULL, NULL),
(1536659774531, '2018-09-10 21:00:00', 4, 'ongoing', NULL, NULL, NULL),
(1536660285552, '2018-09-10 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1536660740259, '2018-09-10 21:00:00', 4, 'ongoing', 600, NULL, NULL),
(1536661358737, '2018-09-10 21:00:00', 4, 'ongoing', 1200, NULL, NULL),
(1536661488767, '2018-09-10 21:00:00', 4, 'ongoing', 3300, NULL, NULL),
(1536661864324, '2018-09-10 21:00:00', 4, 'ongoing', 1800, NULL, NULL),
(1536661875409, '2018-09-10 21:00:00', 4, 'ongoing', 3640, NULL, NULL),
(1536662316655, '2018-09-10 21:00:00', 4, 'ongoing', 600, NULL, NULL),
(1536669812720, '2018-09-10 21:00:00', 4, 'ongoing', 1200, NULL, NULL),
(1536671458638, '2018-09-10 21:00:00', 4, 'ongoing', 1330, NULL, NULL),
(1536671727131, '2018-09-10 21:00:00', 4, 'ongoing', 1200, NULL, NULL),
(1536740973741, '2018-09-11 21:00:00', 4, 'ongoing', 1330, NULL, NULL),
(1536761270172, '2018-09-11 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1536761280914, '2018-09-11 21:00:00', 4, 'ongoing', 390, NULL, NULL),
(1536761463064, '2018-09-11 21:00:00', 1, 'ongoing', 1200, NULL, NULL),
(1536761475474, '2018-09-11 21:00:00', 1, 'ongoing', 390, NULL, NULL),
(1536827270842, '2018-09-12 21:00:00', 7, 'ongoing', 520, NULL, NULL),
(1536827468632, '2018-09-12 21:00:00', 4, 'ongoing', 520, NULL, NULL),
(1536827501503, '2018-09-12 21:00:00', 4, 'ongoing', 1520, NULL, NULL),
(1536827637133, '2018-09-12 21:00:00', 4, 'ongoing', 2120, NULL, NULL),
(1536827703866, '2018-09-12 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1536827728508, '2018-09-12 21:00:00', 4, 'ongoing', 840, NULL, NULL),
(1536828304260, '2018-09-12 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1536828411645, '2018-09-12 21:00:00', 4, 'ongoing', 130, NULL, NULL),
(1536828564955, '2018-09-12 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1536828623400, '2018-09-12 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1536828704345, '2018-09-12 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1536828819073, '2018-09-12 21:00:00', 4, 'ongoing', 520, NULL, NULL),
(1536854154040, '2018-09-12 21:00:00', 1, 'ongoing', 10050, NULL, NULL),
(1536912587762, '2018-09-13 21:00:00', 3, 'ongoing', 2060, NULL, NULL),
(1536912603459, '2018-09-13 21:00:00', 3, 'ongoing', 650, NULL, NULL),
(1536919432339, '2018-09-13 21:00:00', 10, 'ongoing', 1330, NULL, NULL),
(1536927918423, '2018-09-13 21:00:00', 10, 'ongoing', 1800, NULL, NULL),
(1536927966858, '2018-09-13 21:00:00', 10, 'ongoing', 640, NULL, NULL),
(1537169539545, '2018-09-16 21:00:00', 3, 'ongoing', 600, NULL, NULL),
(1537169556616, '2018-09-16 21:00:00', 3, 'ongoing', 740, NULL, NULL),
(1537169686014, '2018-09-16 21:00:00', 3, 'ongoing', 260, NULL, NULL),
(1537188443737, '2018-09-16 21:00:00', 1, 'ongoing', 1200, NULL, NULL),
(1537345675390, '2018-09-18 21:00:00', 4, 'ongoing', 40, NULL, NULL),
(1537345815160, '2018-09-18 21:00:00', 4, 'ongoing', 100, NULL, NULL),
(1537362756799, '2018-09-18 21:00:00', 4, 'ongoing', 710, NULL, NULL),
(1537957424193, '2018-09-25 21:00:00', 4, 'ongoing', 1590, NULL, NULL),
(1537957445271, '2018-09-25 21:00:00', 4, 'ongoing', 550, NULL, NULL),
(1537958241107, '2018-09-25 21:00:00', 1, 'ongoing', 2560, NULL, NULL),
(1537958259637, '2018-09-25 21:00:00', 1, 'ongoing', 160, NULL, NULL),
(1537961442204, '2018-09-25 21:00:00', 4, 'ongoing', 600, NULL, NULL),
(1537961469896, '2018-09-25 21:00:00', 4, 'ongoing', 640, NULL, NULL),
(1538031248984, '2018-09-26 21:00:00', 3, 'ongoing', 580, NULL, NULL),
(1538032965746, '2018-09-26 21:00:00', 3, 'ongoing', 1720, NULL, NULL),
(1538382180586, '2018-09-30 21:00:00', 3, 'ongoing', 920, NULL, NULL),
(1538382198460, '2018-09-30 21:00:00', 3, 'ongoing', 130, NULL, NULL),
(1538382384608, '2018-09-30 21:00:00', 10, 'ongoing', 420, NULL, NULL),
(1538382398848, '2018-09-30 21:00:00', 10, 'ongoing', 160, NULL, NULL),
(1538382406590, '2018-09-30 21:00:00', 10, 'ongoing', 600, NULL, NULL),
(1538389048126, '2018-09-30 21:00:00', 4, 'ongoing', 320, NULL, NULL),
(1538390656913, '2018-09-30 21:00:00', 4, 'ongoing', 600, NULL, NULL),
(1538477138252, '2018-10-01 21:00:00', 4, 'ongoing', 1200, NULL, NULL),
(1538478933599, '2018-10-01 21:00:00', 4, 'ongoing', 260, NULL, NULL),
(1538478945610, '2018-10-01 21:00:00', 4, 'ongoing', 130, NULL, NULL),
(1538478993994, '2018-10-01 21:00:00', 3, 'ongoing', 130, NULL, NULL),
(1538479004969, '2018-10-01 21:00:00', 3, 'ongoing', 520, NULL, NULL),
(1538551955281, '2018-10-02 21:00:00', 4, 'ongoing', 600, NULL, NULL),
(1538552018452, '2018-10-02 21:00:00', 4, 'ongoing', 130, NULL, NULL),
(1538553156766, '2018-10-02 21:00:00', 4, 'ongoing', 2400, NULL, NULL),
(1538553562318, '2018-10-02 21:00:00', 3, 'ongoing', 1200, NULL, NULL),
(1538553570420, '2018-10-02 21:00:00', 3, 'ongoing', 390, NULL, NULL),
(1538559726927, '2018-10-02 21:00:00', 4, 'ongoing', 650, NULL, NULL),
(1538559860353, '2018-10-02 21:00:00', 1, 'ongoing', 760, NULL, NULL),
(1538559880601, '2018-10-02 21:00:00', 1, 'ongoing', 160, NULL, NULL),
(1538565831738, '2018-10-02 21:00:00', 4, 'ongoing', 160, NULL, NULL),
(1538566047904, '2018-10-02 21:00:00', 4, 'ongoing', 600, NULL, NULL),
(1538570858116, '2018-10-02 21:00:00', 4, 'ongoing', 1200, NULL, NULL),
(1538571158869, '2018-10-02 21:00:00', 4, 'ongoing', 260, 0, NULL),
(1538571252725, '2018-10-02 21:00:00', 4, 'ongoing', 1200, 0, NULL),
(1538571543724, '2018-10-02 21:00:00', 4, 'ongoing', 320, 0, NULL),
(1538571899740, '2018-10-02 21:00:00', 4, 'ongoing', 260, 0, NULL),
(1538572066420, '2018-10-02 21:00:00', 4, 'ongoing', 600, 0, NULL),
(1538572133895, '2018-10-02 21:00:00', 4, 'ongoing', 390, 0, NULL),
(1538572519599, '2018-10-02 21:00:00', 4, 'ongoing', 760, 0, NULL),
(1538573269983, '2018-10-02 21:00:00', 4, 'ongoing', 1200, 0, NULL),
(1538573609480, '2018-10-02 21:00:00', 4, 'ongoing', 920, 0, NULL),
(1538574329563, '2018-10-02 21:00:00', 4, 'ongoing', 390, 0, NULL),
(1538574345256, '2018-10-02 21:00:00', 4, 'ongoing', 640, 0, NULL),
(1538574743247, '2018-10-02 21:00:00', 4, 'ongoing', 1680, 1550, NULL),
(1538574863404, '2018-10-02 21:00:00', 1, 'ongoing', 130, 120, NULL),
(1538574872945, '2018-10-02 21:00:00', 1, 'ongoing', 1080, 1000, NULL),
(1538739821172, '2018-10-04 21:00:00', 4, 'ongoing', 320, 300, NULL),
(1538739907491, '2018-10-04 21:00:00', 4, 'ongoing', 320, 300, NULL),
(1538739943604, '2018-10-04 21:00:00', 4, 'ongoing', 160, 150, NULL),
(1538837924511, '2018-10-05 21:00:00', 4, 'ongoing', 1460, 1340, NULL),
(1538838029833, '2018-10-05 21:00:00', 4, 'ongoing', 480, 450, NULL),
(1539068693086, '2018-10-08 21:00:00', 3, 'ongoing', 1330, 1220, NULL),
(1539068709342, '2018-10-08 21:00:00', 3, 'ongoing', 410, 375, NULL),
(1539068746306, '2018-10-08 21:00:00', 1, 'ongoing', 760, 690, NULL),
(1539068770955, '2018-10-08 21:00:00', 1, 'ongoing', 20, 15, NULL),
(1539068779931, '2018-10-08 21:00:00', 1, 'ongoing', 1360, 1250, NULL),
(1539078291474, '2018-10-08 21:00:00', 4, 'ongoing', 1200, 1100, NULL),
(1539178204907, '2018-10-09 21:00:00', 4, 'ongoing', 1590, 1460, NULL),
(1539179676168, '2018-10-09 21:00:00', 1, 'ongoing', 740, 690, NULL),
(1539179695053, '2018-10-09 21:00:00', 1, 'ongoing', 600, 540, NULL),
(1539179730727, '2018-10-09 21:00:00', 3, 'ongoing', 480, 450, NULL),
(1539179742150, '2018-10-09 21:00:00', 3, 'ongoing', 600, 540, NULL),
(1539332507286, '2018-10-11 21:00:00', 3, 'ongoing', 600, 550, NULL),
(1539332516090, '2018-10-11 21:00:00', 3, 'ongoing', 420, 390, NULL),
(1539337728471, '2018-10-11 21:00:00', 4, 'ongoing', 600, 550, NULL),
(1539337834242, '2018-10-11 21:00:00', 4, 'ongoing', 260, 240, NULL),
(1539337989035, '2018-10-11 21:00:00', 4, 'ongoing', 260, 240, NULL),
(1539338166991, '2018-10-11 21:00:00', 4, 'on going', 1200, 1080, NULL),
(1539338222383, '2018-10-11 21:00:00', 4, 'on going', 600, 540, NULL),
(1539338882319, '2018-10-11 21:00:00', 1, 'on going', 40, 30, NULL),
(1539339094843, '2018-10-11 21:00:00', 1, 'ongoing', 20, 15, '2018-10-12 10:11:37'),
(1539339268259, '2018-10-11 21:00:00', 1, 'completed', 390, 360, '2018-10-12 10:14:39'),
(1539339448642, '2018-10-12 10:17:31', 1, 'completed', 600, 550, '2018-10-12 10:17:31'),
(1539339503978, '2018-10-12 10:19:25', 1, 'completed', 920, 840, '2018-10-12 10:19:25'),
(1539340090675, '2018-10-12 10:29:47', 1, 'completed', 130, 120, '2018-10-12 10:29:47'),
(1539340302763, '2018-10-12 10:32:55', 1, 'completed', 860, 780, '2018-10-12 10:32:55'),
(1539340380771, '2018-10-12 10:34:10', 1, 'completed', 600, 550, '2018-10-12 10:34:10');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productcode` varchar(90) NOT NULL,
  `name` varchar(90) NOT NULL,
  `buyingprice` double NOT NULL,
  `sellingprice` double NOT NULL,
  `addedby` int(11) NOT NULL,
  `status` varchar(90) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productcode`, `name`, `buyingprice`, `sellingprice`, `addedby`, `status`, `date`) VALUES
('Elianto1', 'Elianto', 550, 600, 1, NULL, '2018-09-03 11:35:00'),
('Elianto51', 'Elianto', 550, 600, 1, NULL, '2018-09-03 11:17:42'),
('Menengai2b', 'Menengai2', 120, 130, 4, NULL, '2018-09-05 08:20:13'),
('Oliveoil500g1', 'Oliveoil500g', 540, 600, 4, NULL, '2018-10-05 11:35:21'),
('Omo1', 'Omo', 120, 130, 1, NULL, '2018-09-03 11:16:20'),
('Omo2', 'Omo', 120, 130, 4, NULL, '2018-10-05 11:32:17'),
('pampersmid1', 'pampersmid', 150, 160, 4, NULL, '2018-09-05 10:11:37'),
('persil2', 'Persil', 150, 160, 2, NULL, '2018-09-03 12:01:02'),
('Salt500g1', 'Salt500g', 15, 20, 4, NULL, '2018-10-05 11:30:31'),
('Salt500g2', 'Salt500g', 15, 20, 4, NULL, '2018-10-06 14:58:28');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `stockid` varchar(90) NOT NULL,
  `productcode` varchar(90) NOT NULL,
  `amountinstock` int(11) NOT NULL,
  `Status` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stockid`, `productcode`, `amountinstock`, `Status`) VALUES
('Elianto', 'Elianto51', 20, 'available'),
('Menengai2', 'menengai2', 45, 'available'),
('Oliveoil1litr', 'Oliveoil1litr1', 4, 'available'),
('Oliveoil500g', 'Oliveoil500g1', 2, 'available'),
('Omo', 'Omo1', 6, 'available'),
('Pampersmid', 'pampersmid1', -5, 'outof stock'),
('Persil', 'persil2', 15, 'available'),
('Salt500g', 'Salt500g1', 18, 'available');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `description`) VALUES
(1, 'BBidco', 'Blueband'),
(2, 'Kapa', 'Elianto'),
(3, 'mukii', 'mukii'),
(4, 'Supaloaf', 'Bread'),
(5, 'Kapa', 'Ufuta');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `username` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `role` varchar(80) NOT NULL,
  `logintime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `username`, `password`, `role`, `logintime`) VALUES
(1, 'mim', 'mim@yahoo.com', 'mim', 'admin', '2018-10-12 10:06:59'),
(2, 'Kin', 'kin@yahoo.com', 'kin', 'seller', '2018-10-12 08:21:22'),
(3, 'Kin', 'kin@yahoo.com', 'kin', 'seller', '2018-10-12 08:21:22'),
(4, 'Bill', 'bill@yahoo.com', 'bill', 'manager', '2018-10-12 10:36:20'),
(5, 'ggg', 'ggg@yahoo.com', 'ggg', 'null', '2018-07-17 08:22:34'),
(8, 'Dee', 'dee@yahoo.com', 'dee', 'Admin', '2018-09-12 13:12:55'),
(9, 'Fay', 'fay@yahoo.com', 'fay', 'Admin', '2018-07-17 08:22:34'),
(10, 'Eth', 'eth@yahoo.com', 'eth', 'seller', '2018-10-01 08:26:15'),
(12, 'Gih', 'gih@gmail.com', 'gih', 'Admin', '2018-08-02 08:33:19'),
(13, 'Ytt', 'ytt@yahoo.com', 'ytt', 'Seller', '2018-08-02 10:05:18'),
(14, 'Buu', 'buu@yahoo.com', 'buu', 'Manager', '2018-09-18 10:12:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orderlist`
--
ALTER TABLE `orderlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ordernumber` (`ordernumber`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productcode`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`stockid`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orderlist`
--
ALTER TABLE `orderlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=382;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1539340380772;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderlist`
--
ALTER TABLE `orderlist`
  ADD CONSTRAINT `orderlist_ibfk_1` FOREIGN KEY (`ordernumber`) REFERENCES `orders` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`name`) REFERENCES `stock` (`stockid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
