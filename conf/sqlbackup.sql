-- MySQL dump 10.16  Distrib 10.2.17-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: shop
-- ------------------------------------------------------
-- Server version	10.2.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `orderlist`
--

DROP TABLE IF EXISTS `orderlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordernumber` bigint(20) NOT NULL,
  `productcode` varchar(90) NOT NULL,
  `name` varchar(90) NOT NULL,
  `sellingprice` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` double DEFAULT NULL,
  `profit` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordernumber` (`ordernumber`),
  CONSTRAINT `orderlist_ibfk_1` FOREIGN KEY (`ordernumber`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderlist`
--

LOCK TABLES `orderlist` WRITE;
/*!40000 ALTER TABLE `orderlist` DISABLE KEYS */;
INSERT INTO `orderlist` VALUES (7,1533548502719,'Elianto51','Elianto',520,4,2080,NULL),(8,1533548502719,'Soko4','Soko',110,1,110,NULL),(9,1533548502719,'Soko4','Soko',110,3,330,NULL),(15,1533634751327,'Soko4','Soko',110,6,660,NULL),(16,1533634751327,'Oliveb1','Olive oil',975,2,1950,NULL),(17,1533634751327,'Oliveb1','Olive oil',975,6,5850,NULL),(18,1533634751327,'Soko3','Soko',110,2,220,NULL),(19,1533634751327,'Persil2a','Persil',160,3,480,NULL),(20,1533634751327,'Persil2a','Persil',160,1,160,NULL),(34,1533727511620,'Oliveb1','Olive oil',975,2,1950,NULL),(35,1533727511620,'Omo1','Omo',230,2,460,NULL),(36,1533727511620,'Persil2a','Persil',160,1,160,NULL),(37,1533727511620,'Soko3','Soko',110,2,220,NULL),(39,1533728002396,'Soko4','Soko',110,1,110,NULL),(40,1533730995899,'Steelwire2','Steelwire',15,3,45,NULL),(41,1533731020136,'Persil2a','Persil',160,4,640,NULL),(42,1533731207229,'Oliveb1','Olive oil',975,2,1950,NULL),(43,1533731228597,'Persil2a','Persil',160,2,320,NULL),(45,1534155019518,'Soko3','Soko',110,2,220,NULL),(46,1534155019518,'Pampersb3','Pampers',850,3,2550,NULL),(48,1534155205722,'Soko4','Soko',110,1,110,NULL),(49,1534155205722,'Steelwire2','Steelwire',15,1,15,NULL),(50,1534155205722,'Jogoo1','Jogoo',115,1,115,NULL),(53,1534156099909,'Jogoo1','Jogoo',115,1,115,NULL),(54,1534156099909,'Pampersb3','Pampers',850,2,1700,NULL),(57,1534156213902,'Omo1','Omo',230,3,690,NULL),(58,1534156213902,'Oliveb1','Olive oil',975,1,975,NULL),(69,1534159275831,'Oliveb1','Olive oil',975,1,975,NULL),(70,1534159275831,'Pampersb3','Pampers',850,3,2550,NULL),(71,1534159315777,'Soko3','Soko',110,2,220,NULL),(72,1534159315777,'Steelwire2','Steelwire',15,2,30,NULL),(73,1534159315777,'Soko3','Soko',110,1,110,NULL),(74,1534159315777,'Soko3','Soko',110,4,440,NULL),(75,1534159315777,'Soko3','Soko',110,1,110,NULL),(76,1534159315777,'Soko3','Soko',110,4,440,NULL),(77,1534159315777,'Elianto51','Elianto',520,2,1040,NULL),(78,1534159315777,'Steelwire2','Steelwire',15,1,NULL,NULL),(79,1534159315777,'Omo1','Omo',3,0,NULL,NULL),(80,1534159315777,'Soko4','Soko',0,0,NULL,NULL),(81,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(82,1534159315777,'Soko4','Soko',2,0,NULL,NULL),(83,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(84,1534159315777,'Soko4','Soko',2,0,NULL,NULL),(85,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(86,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(87,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(88,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(89,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(90,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(91,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(92,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(93,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(94,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(95,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(96,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(97,1534159315777,'Soko4','Soko',1,0,NULL,NULL),(98,1534244746125,'Soko4','Soko',110,1,NULL,NULL),(99,1534244931659,'Oliveb1','Olive oil',975,1,NULL,NULL),(100,1534244931659,'Oliveb1','Olive oil',975,1,NULL,NULL),(101,1534244931659,'Pampersb3','Pampers',850,2,NULL,NULL),(102,1534244931659,'Pampersb3','Pampers',850,4,NULL,NULL),(103,1534246438084,'Oliveb1','Olive oil',975,1,975,NULL),(104,1534246438084,'Soko3','Soko',110,3,330,NULL),(105,1534324680617,'Steelwire2','Steelwire',15,1,15,NULL),(106,1534324705254,'Omo1','Omo',230,1,230,NULL),(107,1534324705254,'Steelwire2','Steelwire',15,1,15,NULL),(108,1534325511619,'Omo1','Omo',230,1,230,NULL),(109,1534325511619,'Omo1','Omo',230,2,460,NULL),(110,1534325511619,'Omo1','Omo',230,2,460,NULL),(111,1534326250732,'Omo1','Omo',230,3,690,NULL),(112,1534326250732,'Omo1','Omo',230,1,230,NULL),(113,1534326250732,'Omo1','Omo',230,3,690,NULL),(114,1534326250732,'Omo1','Omo',230,2,460,NULL),(115,1534326250732,'Omo1','Omo',230,1,230,NULL),(116,1534326250732,'Omo1','Omo',230,2,460,NULL),(117,1534326250732,'Omo1','Omo',230,1,230,NULL),(118,1534328695110,'Omo1','Omo',230,2,460,NULL),(119,1534328695110,'Omo1','Omo',230,1,230,NULL),(120,1534328695110,'Omo1','Omo',230,3,690,NULL),(121,1534328695110,'Omo1','Omo',230,3,690,NULL),(122,1534328695110,'Omo1','Omo',230,1,230,NULL),(123,1534328695110,'Omo1','Omo',230,1,230,NULL),(124,1534328695110,'Omo1','Omo',230,1,230,NULL),(125,1534328695110,'Omo1','Omo',230,2,460,NULL),(126,1534328695110,'Omo1','Omo',230,3,690,NULL),(127,1534333165285,'Omo1','Omo',230,2,460,NULL),(128,1534333256854,'Jogoo2','Jogoo',115,2,230,NULL),(129,1534333256854,'Omo1','Omo',230,1,230,NULL),(130,1534333256854,'Omo1','Omo',230,3,690,NULL),(131,1534333256854,'Omo1','Omo',230,2,460,NULL),(132,1534334915808,'Jogoo1','Jogoo',115,2,230,NULL),(133,1534334915808,'Oliveb1','Olive oil',975,1,975,NULL),(134,1534334915808,'Pampersb3','Pampers',850,3,2550,NULL),(135,1535974252706,'Omo1','Omo',130,1,130,NULL),(136,1535975881615,'Elianto51','Elianto',600,2,1200,NULL),(137,1535990216257,'Elianto1','Elianto',600,7,4200,NULL),(138,1535990216257,'Elianto1','Elianto',600,8,4800,NULL),(139,1535990216257,'Elianto1','Elianto',600,8,4800,NULL),(140,1536047261188,'Omo1','Omo',130,1,130,NULL),(141,1536047261188,'Elianto1','Elianto',600,2,1200,NULL),(142,1536047261188,'persil2','Persil',160,2,320,NULL),(143,1536047261188,'persil2','Persil',160,2,320,NULL),(144,1536047261188,'Omo1','Omo',130,2,260,NULL),(145,1536054361268,'persil2','Persil',160,3,480,NULL),(146,1536054361268,'persil2','Persil',160,2,320,NULL),(147,1536054361268,'Elianto1','Elianto',600,3,1800,NULL),(148,1536054361268,'Elianto51','Elianto',600,1,600,NULL),(149,1536054361268,'Elianto1','Elianto',600,3,1800,NULL),(150,1536054361268,'Omo1','Omo',130,1,130,NULL),(151,1536158640390,'Elianto','Elianto51',230,2,460,NULL),(159,1536418644841,'Elianto51','Elianto',600,0,0,NULL),(160,1536418644841,'Omo1','Omo',130,78,10140,NULL),(161,1536418644841,'Omo1','Omo',130,677,88010,NULL),(162,1536418644841,'Omo1','Omo',130,898,116740,NULL),(163,1536418644841,'Menengai2b','Menengai2',130,879,114270,NULL),(164,1536418644841,'Menengai2b','Menengai2',130,78,10140,NULL);
/*!40000 ALTER TABLE `orderlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_date` date DEFAULT NULL,
  `order_status` varchar(2000) DEFAULT NULL,
  `numberofitems` int(11) DEFAULT NULL,
  `ordertotalcost` int(11) DEFAULT NULL,
  `order_seller_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1536418644842 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1533548502719,'2018-09-08','compleated',NULL,NULL,NULL),(1533634751327,'2018-09-08','compleated',NULL,NULL,NULL),(1533718361338,'2018-09-08','compleated',NULL,NULL,NULL),(1533727511620,'2018-09-08','compleated',NULL,NULL,NULL),(1533728002396,'2018-09-08','compleated',NULL,NULL,NULL),(1533730995899,'2018-09-08','compleated',NULL,NULL,NULL),(1533731020136,'2018-09-08','compleated',NULL,NULL,NULL),(1533731207229,'2018-09-08','compleated',NULL,NULL,NULL),(1533731228597,'2018-09-08','compleated',NULL,NULL,NULL),(1534155019518,'2018-09-08','compleated',NULL,NULL,NULL),(1534155205722,'2018-09-08','compleated',NULL,NULL,NULL),(1534155913538,'2018-09-08','compleated',NULL,NULL,NULL),(1534156099909,'2018-09-08','compleated',NULL,NULL,NULL),(1534156213902,'2018-09-08','compleated',NULL,NULL,NULL),(1534156213903,'2018-09-08','compleated',NULL,NULL,NULL),(1534156213904,'2018-09-08','compleated',NULL,NULL,NULL),(1534159275831,'2018-09-08','compleated',NULL,NULL,NULL),(1534159315777,'2018-09-08','compleated',NULL,NULL,NULL),(1534244746125,'2018-09-08','compleated',NULL,NULL,NULL),(1534244931659,'2018-09-08','compleated',NULL,NULL,NULL),(1534246438084,'2018-09-08','compleated',NULL,NULL,NULL),(1534324680617,'2018-09-08','compleated',NULL,NULL,NULL),(1534324705254,'2018-09-08','compleated',NULL,NULL,NULL),(1534325511619,'2018-09-08','compleated',NULL,NULL,NULL),(1534326250732,'2018-09-08','compleated',NULL,NULL,NULL),(1534328695110,'2018-09-08','compleated',NULL,NULL,NULL),(1534333165285,'2018-09-08','compleated',NULL,NULL,NULL),(1534333256854,'2018-09-08','compleated',NULL,NULL,NULL),(1534334915808,'2018-09-08','compleated',NULL,NULL,NULL),(1535974252706,'2018-09-08','compleated',NULL,NULL,NULL),(1535975881615,'2018-09-08','compleated',NULL,NULL,NULL),(1535990216257,'2018-09-08','compleated',NULL,NULL,NULL),(1535991314907,'2018-09-08','compleated',NULL,NULL,NULL),(1536047261188,'2018-09-08','compleated',NULL,NULL,NULL),(1536054361268,'2018-09-08','compleated',NULL,NULL,NULL),(1536158640390,'2018-09-08','compleated',NULL,NULL,NULL),(1536418644841,'2018-09-08','on going',NULL,NULL,4);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `productcode` varchar(90) NOT NULL,
  `name` varchar(90) NOT NULL,
  `buyingprice` double NOT NULL,
  `sellingprice` double NOT NULL,
  `addedby` int(11) NOT NULL,
  `status` varchar(90) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`productcode`),
  KEY `name` (`name`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`name`) REFERENCES `stock` (`stockid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES ('Elianto51','Elianto',550,600,1,NULL,'2018-09-03 11:17:42'),('Elianto52','Elianto',550,600,2,NULL,'2018-09-03 11:33:33'),('Menengai2b','Menengai2',120,130,4,NULL,'2018-09-05 08:20:13'),('Omo1','Omo',120,130,1,NULL,'2018-09-03 11:16:20'),('pampersmid1','pampersmid',150,160,4,NULL,'2018-09-05 10:11:37'),('persil2','Persil',150,160,2,NULL,'2018-09-03 12:01:02');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `stockid` varchar(90) NOT NULL,
  `productcode` varchar(90) NOT NULL,
  `amountinstock` int(11) NOT NULL,
  `Status` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`stockid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock`
--

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` VALUES ('Elianto','Elianto51',-1342,'Available'),('Menengai2','menengai2',4,'available'),('Omo','Omo1',5,'available'),('Pampersmid','pampersmid1',10,'available'),('Persil','persil2',4,'Available'),('Salt500g','Salt500g1',15,'available');
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'BBidco','Blueband'),(2,'Kapa','Elianto'),(3,'mukii','mukii'),(4,'Supaloaf','Bread');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `username` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `role` varchar(80) NOT NULL,
  `logintime` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'mim','mim@yahoo.com','mim','admin','2018-07-19 11:36:35'),(2,'Kin','kin@yahoo.com','kin','seller','2018-08-01 10:06:17'),(3,'Kin','kin@yahoo.com','kin','seller','2018-08-01 10:06:17'),(4,'Bill','bill@yahoo.com','bill','manager','2018-09-08 12:44:41'),(5,'ggg','ggg@yahoo.com','ggg','null','2018-07-17 08:22:34'),(7,'kin','kin@yahoo.com','kin','Manager','2018-08-01 10:06:17'),(8,'Dee','dee@yahoo.com','dee','Admin','2018-07-19 08:28:30'),(9,'Fay','fay@yahoo.com','fay','Admin','2018-07-17 08:22:34'),(10,'Eth','eth@yahoo.com','eth','seller','2018-07-17 08:22:34'),(11,'fgg','jjjj@yahoo.com','ghhh','Seller','2018-07-25 13:30:40'),(12,'Gih','gih@gmail.com','gih','Admin','2018-08-02 08:33:19'),(13,'Ytt','ytt@yahoo.com','ytt','Seller','2018-08-02 10:05:18'),(14,'Full Ntestame','johnmukii@yahoo.com','37776','Manager','2018-09-07 11:34:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-10 10:32:37
