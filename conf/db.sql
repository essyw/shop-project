-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 06, 2018 at 01:23 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--
CREATE DATABASE IF NOT EXISTS `shop` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `shop`;

-- --------------------------------------------------------

--
-- Table structure for table `orderlist`
--

CREATE TABLE `orderlist` (
  `id` int(11) NOT NULL,
  `ordernumber` bigint(20) NOT NULL,
  `productcode` varchar(90) NOT NULL,
  `name` varchar(90) NOT NULL,
  `sellingprice` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderlist`
--

INSERT INTO `orderlist` (`id`, `ordernumber`, `productcode`, `name`, `sellingprice`, `quantity`, `total`) VALUES
(7, 1533548502719, 'Elianto51', 'Elianto', 520, 4, 2080),
(8, 1533548502719, 'Soko4', 'Soko', 110, 1, 110),
(9, 1533548502719, 'Soko4', 'Soko', 110, 3, 330),
(15, 1533634751327, 'Soko4', 'Soko', 110, 6, 660),
(16, 1533634751327, 'Oliveb1', 'Olive oil', 975, 2, 1950),
(17, 1533634751327, 'Oliveb1', 'Olive oil', 975, 6, 5850),
(18, 1533634751327, 'Soko3', 'Soko', 110, 2, 220),
(19, 1533634751327, 'Persil2a', 'Persil', 160, 3, 480),
(20, 1533634751327, 'Persil2a', 'Persil', 160, 1, 160),
(34, 1533727511620, 'Oliveb1', 'Olive oil', 975, 2, 1950),
(35, 1533727511620, 'Omo1', 'Omo', 230, 2, 460),
(36, 1533727511620, 'Persil2a', 'Persil', 160, 1, 160),
(37, 1533727511620, 'Soko3', 'Soko', 110, 2, 220),
(39, 1533728002396, 'Soko4', 'Soko', 110, 1, 110),
(40, 1533730995899, 'Steelwire2', 'Steelwire', 15, 3, 45),
(41, 1533731020136, 'Persil2a', 'Persil', 160, 4, 640),
(42, 1533731207229, 'Oliveb1', 'Olive oil', 975, 2, 1950),
(43, 1533731228597, 'Persil2a', 'Persil', 160, 2, 320),
(45, 1534155019518, 'Soko3', 'Soko', 110, 2, 220),
(46, 1534155019518, 'Pampersb3', 'Pampers', 850, 3, 2550),
(48, 1534155205722, 'Soko4', 'Soko', 110, 1, 110),
(49, 1534155205722, 'Steelwire2', 'Steelwire', 15, 1, 15),
(50, 1534155205722, 'Jogoo1', 'Jogoo', 115, 1, 115),
(53, 1534156099909, 'Jogoo1', 'Jogoo', 115, 1, 115),
(54, 1534156099909, 'Pampersb3', 'Pampers', 850, 2, 1700),
(57, 1534156213902, 'Omo1', 'Omo', 230, 3, 690),
(58, 1534156213902, 'Oliveb1', 'Olive oil', 975, 1, 975),
(69, 1534159275831, 'Oliveb1', 'Olive oil', 975, 1, 975),
(70, 1534159275831, 'Pampersb3', 'Pampers', 850, 3, 2550),
(71, 1534159315777, 'Soko3', 'Soko', 110, 2, 220),
(72, 1534159315777, 'Steelwire2', 'Steelwire', 15, 2, 30),
(73, 1534159315777, 'Soko3', 'Soko', 110, 1, 110),
(74, 1534159315777, 'Soko3', 'Soko', 110, 4, 440),
(75, 1534159315777, 'Soko3', 'Soko', 110, 1, 110),
(76, 1534159315777, 'Soko3', 'Soko', 110, 4, 440),
(77, 1534159315777, 'Elianto51', 'Elianto', 520, 2, 1040),
(78, 1534159315777, 'Steelwire2', 'Steelwire', 15, 1, NULL),
(79, 1534159315777, 'Omo1', 'Omo', 3, 0, NULL),
(80, 1534159315777, 'Soko4', 'Soko', 0, 0, NULL),
(81, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(82, 1534159315777, 'Soko4', 'Soko', 2, 0, NULL),
(83, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(84, 1534159315777, 'Soko4', 'Soko', 2, 0, NULL),
(85, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(86, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(87, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(88, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(89, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(90, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(91, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(92, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(93, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(94, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(95, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(96, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(97, 1534159315777, 'Soko4', 'Soko', 1, 0, NULL),
(98, 1534244746125, 'Soko4', 'Soko', 110, 1, NULL),
(99, 1534244931659, 'Oliveb1', 'Olive oil', 975, 1, NULL),
(100, 1534244931659, 'Oliveb1', 'Olive oil', 975, 1, NULL),
(101, 1534244931659, 'Pampersb3', 'Pampers', 850, 2, NULL),
(102, 1534244931659, 'Pampersb3', 'Pampers', 850, 4, NULL),
(103, 1534246438084, 'Oliveb1', 'Olive oil', 975, 1, 975),
(104, 1534246438084, 'Soko3', 'Soko', 110, 3, 330),
(105, 1534324680617, 'Steelwire2', 'Steelwire', 15, 1, 15),
(106, 1534324705254, 'Omo1', 'Omo', 230, 1, 230),
(107, 1534324705254, 'Steelwire2', 'Steelwire', 15, 1, 15),
(108, 1534325511619, 'Omo1', 'Omo', 230, 1, 230),
(109, 1534325511619, 'Omo1', 'Omo', 230, 2, 460),
(110, 1534325511619, 'Omo1', 'Omo', 230, 2, 460),
(111, 1534326250732, 'Omo1', 'Omo', 230, 3, 690),
(112, 1534326250732, 'Omo1', 'Omo', 230, 1, 230),
(113, 1534326250732, 'Omo1', 'Omo', 230, 3, 690),
(114, 1534326250732, 'Omo1', 'Omo', 230, 2, 460),
(115, 1534326250732, 'Omo1', 'Omo', 230, 1, 230),
(116, 1534326250732, 'Omo1', 'Omo', 230, 2, 460),
(117, 1534326250732, 'Omo1', 'Omo', 230, 1, 230),
(118, 1534328695110, 'Omo1', 'Omo', 230, 2, 460),
(119, 1534328695110, 'Omo1', 'Omo', 230, 1, 230),
(120, 1534328695110, 'Omo1', 'Omo', 230, 3, 690),
(121, 1534328695110, 'Omo1', 'Omo', 230, 3, 690),
(122, 1534328695110, 'Omo1', 'Omo', 230, 1, 230),
(123, 1534328695110, 'Omo1', 'Omo', 230, 1, 230),
(124, 1534328695110, 'Omo1', 'Omo', 230, 1, 230),
(125, 1534328695110, 'Omo1', 'Omo', 230, 2, 460),
(126, 1534328695110, 'Omo1', 'Omo', 230, 3, 690),
(127, 1534333165285, 'Omo1', 'Omo', 230, 2, 460),
(128, 1534333256854, 'Jogoo2', 'Jogoo', 115, 2, 230),
(129, 1534333256854, 'Omo1', 'Omo', 230, 1, 230),
(130, 1534333256854, 'Omo1', 'Omo', 230, 3, 690),
(131, 1534333256854, 'Omo1', 'Omo', 230, 2, 460),
(132, 1534334915808, 'Jogoo1', 'Jogoo', 115, 2, 230),
(133, 1534334915808, 'Oliveb1', 'Olive oil', 975, 1, 975),
(134, 1534334915808, 'Pampersb3', 'Pampers', 850, 3, 2550),
(135, 1535974252706, 'Omo1', 'Omo', 130, 1, 130),
(136, 1535975881615, 'Elianto51', 'Elianto', 600, 2, 1200),
(137, 1535990216257, 'Elianto1', 'Elianto', 600, 7, 4200),
(138, 1535990216257, 'Elianto1', 'Elianto', 600, 8, 4800),
(139, 1535990216257, 'Elianto1', 'Elianto', 600, 8, 4800),
(140, 1536047261188, 'Omo1', 'Omo', 130, 1, 130),
(141, 1536047261188, 'Elianto1', 'Elianto', 600, 2, 1200),
(142, 1536047261188, 'persil2', 'Persil', 160, 2, 320),
(143, 1536047261188, 'persil2', 'Persil', 160, 2, 320),
(144, 1536047261188, 'Omo1', 'Omo', 130, 2, 260),
(145, 1536054361268, 'persil2', 'Persil', 160, 3, 480),
(146, 1536054361268, 'persil2', 'Persil', 160, 2, 320),
(147, 1536054361268, 'Elianto1', 'Elianto', 600, 3, 1800),
(148, 1536054361268, 'Elianto51', 'Elianto', 600, 1, 600),
(149, 1536054361268, 'Elianto1', 'Elianto', 600, 3, 1800),
(150, 1536054361268, 'Omo1', 'Omo', 130, 1, 130),
(151, 1536158640390, 'Elianto', 'Elianto51', 230, 2, 460);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`) VALUES
(1533548502719),
(1533634751327),
(1533718361338),
(1533727511620),
(1533728002396),
(1533730995899),
(1533731020136),
(1533731207229),
(1533731228597),
(1534155019518),
(1534155205722),
(1534155913538),
(1534156099909),
(1534156213902),
(1534156213903),
(1534156213904),
(1534159275831),
(1534159315777),
(1534244746125),
(1534244931659),
(1534246438084),
(1534324680617),
(1534324705254),
(1534325511619),
(1534326250732),
(1534328695110),
(1534333165285),
(1534333256854),
(1534334915808),
(1535974252706),
(1535975881615),
(1535990216257),
(1535991314907),
(1536047261188),
(1536054361268),
(1536158640390);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productcode` varchar(90) NOT NULL,
  `name` varchar(90) NOT NULL,
  `buyingprice` double NOT NULL,
  `sellingprice` double NOT NULL,
  `addedby` int(11) NOT NULL,
  `status` varchar(90) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productcode`, `name`, `buyingprice`, `sellingprice`, `addedby`, `status`, `date`) VALUES
('Elianto1', 'Elianto', 550, 600, 1, NULL, '2018-09-03 11:35:00'),
('Elianto51', 'Elianto', 550, 600, 1, NULL, '2018-09-03 11:17:42'),
('Elianto52', 'Elianto', 550, 600, 2, NULL, '2018-09-03 11:33:33'),
('Menengai2b', 'Menengai2', 120, 130, 4, NULL, '2018-09-05 08:20:13'),
('Omo1', 'Omo', 120, 130, 1, NULL, '2018-09-03 11:16:20'),
('pampersmid1', 'pampersmid', 150, 160, 4, NULL, '2018-09-05 10:11:37'),
('persil2', 'Persil', 150, 160, 2, NULL, '2018-09-03 12:01:02');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `stockid` varchar(90) NOT NULL,
  `productcode` varchar(90) NOT NULL,
  `amountinstock` int(11) NOT NULL,
  `Status` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stockid`, `productcode`, `amountinstock`, `Status`) VALUES
('Elianto', 'Elianto51', 18, 'Available'),
('Menengai2', 'menengai2', 4, 'available'),
('Omo', 'Omo1', 5, 'available'),
('Pampersmid', 'pampersmid1', 10, 'available'),
('Persil', 'persil2', 4, 'Available'),
('Salt500g', 'Salt500g1', 15, 'available');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `description`) VALUES
(1, 'BBidco', 'Blueband'),
(2, 'Kapa', 'Elianto'),
(3, 'mukii', 'mukii'),
(4, 'Supaloaf', 'Bread');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `username` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `role` varchar(80) NOT NULL,
  `logintime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `username`, `password`, `role`, `logintime`) VALUES
(1, 'mim', 'mim@yahoo.com', 'mim', 'admin', '2018-07-19 11:36:35'),
(2, 'Kin', 'kin@yahoo.com', 'kin', 'seller', '2018-08-01 10:06:17'),
(3, 'Kin', 'kin@yahoo.com', 'kin', 'seller', '2018-08-01 10:06:17'),
(4, 'Bill', 'bill@yahoo.com', 'bill', 'manager', '2018-09-05 10:05:39'),
(5, 'ggg', 'ggg@yahoo.com', 'ggg', 'null', '2018-07-17 08:22:34'),
(7, 'kin', 'kin@yahoo.com', 'kin', 'Manager', '2018-08-01 10:06:17'),
(8, 'Dee', 'dee@yahoo.com', 'dee', 'Admin', '2018-07-19 08:28:30'),
(9, 'Fay', 'fay@yahoo.com', 'fay', 'Admin', '2018-07-17 08:22:34'),
(10, 'Eth', 'eth@yahoo.com', 'eth', 'seller', '2018-07-17 08:22:34'),
(11, 'fgg', 'jjjj@yahoo.com', 'ghhh', 'Seller', '2018-07-25 13:30:40'),
(12, 'Gih', 'gih@gmail.com', 'gih', 'Admin', '2018-08-02 08:33:19'),
(13, 'Ytt', 'ytt@yahoo.com', 'ytt', 'Seller', '2018-08-02 10:05:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orderlist`
--
ALTER TABLE `orderlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ordernumber` (`ordernumber`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productcode`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`stockid`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orderlist`
--
ALTER TABLE `orderlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1536158640391;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderlist`
--
ALTER TABLE `orderlist`
  ADD CONSTRAINT `orderlist_ibfk_1` FOREIGN KEY (`ordernumber`) REFERENCES `orders` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`name`) REFERENCES `stock` (`stockid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
