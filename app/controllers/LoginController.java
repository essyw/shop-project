package controllers;

import play.mvc.*;

import views.html.*;
import services.ShopOperations;
import services.Login;
import services.Log;
import services.Users;
import services.Products;
import services.Suppliers;
import services.Stock;
import services.ResetPassword;
import java.util.*;
import java.text.SimpleDateFormat;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
import services.ShopOperations;
import services.Login;
import services.Log;
import services.Users;
import services.Products;
import services.Suppliers;
import services.Stock;
import services.ResetPassword;
import java.util.*;
import java.text.SimpleDateFormat;
import javax.inject.*;
import views.html.*;
import play.data.*;
public class LoginController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

private ShopOperations shopop;
private play.data.FormFactory fmm;
@Inject
    public LoginController(ShopOperations shopop,play.data.FormFactory fmm) {
        this.shopop = shopop;
        this.fmm=fmm;
    }

    public Result login() {

        if(request().method().equals("GET")){
            if(session("username")!=null&&!session("username").isEmpty()) {

                //user already  logged  in  take  them  to home  page
               // return  redirect("/home");
               return ok(login.render());
            }else{
                //user  not  yet authorized  take  them  to  login page
                return ok(login.render());
            }
        }else{
            DynamicForm requestData = fmm.form().bindFromRequest();
            String username = requestData.get("userName");
            String password= requestData.get("password");
            Login log = new Login(username,password);
            Login logy=shopop.getlogin(log);
            //check if  user user  logged  in sucessfully
        //by checking  if logy is  null
        if( logy.getRole().equals("manager"))
        {
            String x =logy.getName();
            session("connected",x);
            session("name",x);
            session("username",logy.getUsername());
            session("userid",logy.getId()+"");
            String user =session().get("connected");
            System.out.println("..."+user);
            String role = logy.getRole();
            session("role",role);
            System.out.println(">>>>>>>>>>>>"+role);
            return redirect("/home");
        }else{
                
            
        if(logy!=null){
            String x =logy.getName();
            session("connected",x);
            session("name",x);
            session("username",logy.getUsername());
            session("userid",logy.getId()+"");
            String user =session().get("connected");
            System.out.println("..."+user);
            String role = logy.getRole();
        session("role",role);
            System.out.println(">>>>>>>>>>>>"+role);
        return redirect("/managerr");
        }else{
            //login failed
        return redirect("/login");
        }
        }
    }
}
}
