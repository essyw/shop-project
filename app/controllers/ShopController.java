package controllers;

import play.mvc.*;

import views.html.*;
import play.data.*;
import play.db.*;
import javax.inject.*;
import services.ShopOperations;
import services.Login;
import services.Log;
import services.Users;
import services.Products;
import services.Suppliers;
import services.Stock;
import services.Todaysale;
import services.Daysale;
import services.OrderList;
import services.Monthlysale;
import services.ResetPassword;
import java.util.*; 
import java.text.SimpleDateFormat;
import services.AdminSecurity;
import services.AdminOnlySecurity;
import java.math.BigInteger;
import java.util.Calendar;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
@Security.Authenticated(AdminSecurity.class)
public class ShopController extends Controller {
   private ShopOperations shopop;
   play.data.FormFactory fmm;
   @Inject
 public ShopController(ShopOperations shopop, play.data.FormFactory fmm){ 

       this.shopop=shopop;
          this.fmm=fmm; 
 } 
    public Result mainpage() {
        return ok(mainpage.render());
    }
    public Result header() {
   
        return ok(header.render());
    }
    public Result home() {
        Daysale day = new Daysale();
        Todaysale today = new Todaysale();
        Stock stock = new Stock();
        Users us = new Users();
        Products product = new Products();
        ArrayList<OrderList> order = new ArrayList<OrderList>();
        ArrayList<Users> user = shopop.allsellers();
        ArrayList<Daysale> todaysale=shopop.gettodaysale(day);
       int todayscount=shopop.todaysalecount(today);
       shopop.outofStock(stock);
       int x=shopop.allproductcount();
       product.setCount(x);
       double profit=shopop. getProfit();
       day.setProfit(profit);
      double cost= shopop. getCost();
      day.setCost(cost);
        return ok(home.render(todaysale,day,today,user,stock,product,us,order));
    }
    public Result log() {
        //if(request().method().equals("GET")){
         //   return ok(log.render());  
        //}else{
      //  Form<Log> logform = fmm.form(Log.class);
       // Log l =logform.bindFromRequest().get();
       // System.out.println("Username>>"+l.username);
       // System.out.println("password>>>"+l.password);
         
            
            return redirect("/log");
        } 
    
    

    public Result logout(){
        
        session().clear();
          return redirect("/login");
     }
    
    public Result adduser() {

        ArrayList<Users> user = shopop.allusers();
        if(request().method().equals("GET")){
            return ok(adduser.render(user)); 
        }else{
            DynamicForm requestData = fmm.form().bindFromRequest();
            Users newUser= new Users(requestData.get("name"),requestData.get("userName"),
            requestData.get("password"),requestData.get("role"));
           boolean ans= shopop.addUser(newUser); 
           if(ans == true){

            flash("message","User Registered");
            return  redirect("/adduser");
        }else{
            flash("message","User Registration  failed");
          return  redirect("/adduser");
        }
        }
    }
    public Result edituser(String id,String username,String role) {
        ArrayList<Users> users = shopop.allusers();
       Users user = new Users(Integer.parseInt(id),username,role);
       if(request().method().equals("GET")){
        return ok(edituser.render(user,users));
    } else{
        DynamicForm requestData = fmm.form().bindFromRequest();
        String newid=requestData.get("id");
        int d=Integer.parseInt(newid);
        Users newUser= new Users(d,requestData.get("username"),
           requestData.get("role"));
           shopop.editUser(newUser);
    }
            return ok(edituser.render(user,users));
        }
     public Result deleteuser(String id,String username,String role) {
        Users user = new Users(Integer.parseInt(id),username,role);
        shopop.deletUser(user);
        return  redirect("/adduser");
          }

    public Result addproduct() {   
        Stock s = new Stock(); 
        ArrayList<Products> prodd = shopop.allproducts();
        ArrayList<Stock> stok =shopop.allStockid();
        //you are  printing the  object  not  the  data  inside  the  array   list

        System.out.println("Size  of  your  arraylist>>"+stok.size()+"The stockid for  first item is >>"+stok.get(0).getStockid());
        //the print  a null,

        if(request().method().equals("GET")){
            return ok(addproduct.render(prodd,stok,s));
        } else{
            DynamicForm requestData = fmm.form().bindFromRequest();
             String buyingprice= requestData.get("buyingprice");
             String sellingprice= requestData.get("sellingprice");
             String addedby =requestData.get("addedby");
             double bp =Double.parseDouble(buyingprice);
             double sp =Double.parseDouble(sellingprice);
             int ab =Integer.parseInt(addedby);
            Products prod = new Products(requestData.get("productcode"),requestData.get("value"),    
       bp,sp,ab);
       boolean ans=shopop.addProduct(prod);
       if(ans == true){

        flash("message","product added");
        return  redirect("/addproduct");
    }else{
        flash("message","addition failed");
      return  redirect("/addproduct");
    }
    }
 }
    public Result allproducts() {
        int quantity=0;
        long y=0;
        Products pr = new Products();
        String currentsession =session().get("selltime");
        try{
         y=Long.parseLong((session().get("selltime")));
        }catch(NumberFormatException e){
        }          
        System.out.println("...my current session  is in allproductaction.."+currentsession);
        double totalcost=  shopop.totalSales(y,pr,quantity);
          pr.setTotalcost(totalcost);
          ArrayList<Products> prod = shopop.allproducts(); 
          ArrayList<Products> pro=shopop.orderedproducts(currentsession); 
            return ok(allproducts.render(prod,pr,pro));
        }

    @Security.Authenticated(AdminOnlySecurity.class)
    public Result deleteproduct(String code,String name) {
            Products prod = new Products(code,name);
            shopop.deleteproduct(code);
            String m =prod.getProductcode();
            //System.out.println("............."+m); 
            return  redirect("/allproducts");
             // return ok(deleteproduct.render(prod));
          }
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result editproduct(String code,String name,String buyingprice,String sellingprice) {
        Products prodd = new Products(code,name,Double.parseDouble(buyingprice),Double.parseDouble(sellingprice));
        if(request().method().equals("GET")){
            System.out.println("method>>>>>"+request().method());

            return ok(editproduct.render(prodd));
        } else{
        DynamicForm requestData = fmm.form().bindFromRequest();
        
             String selling= requestData.get("sellingprice");
             System.out.println(selling);
             String buying= requestData.get("buyingprice");
             System.out.println(buying);
             double bp =Double.parseDouble(buying);
             double sp =Double.parseDouble(selling);
            Products prod = new Products(requestData.get("productcode"),requestData.get("name"),    
       bp,sp);
        shopop.editproduct(prod);
         return ok(editproduct.render(prod));
        }
          }

    @Security.Authenticated(AdminOnlySecurity.class)
     public Result addsupplier() {
         ArrayList<Suppliers> supplier=shopop.allSuppliers();
          if(request().method().equals("GET")){
           return ok(addsupplier.render(supplier));
           }else{
        DynamicForm requestData = fmm.form().bindFromRequest();
        Suppliers sup = new Suppliers(requestData.get("name"),requestData.get("description"));
        boolean ans = shopop.addSupplier(sup);
        if(ans == true){

            flash("message","supplier added");
            return  redirect("/addsupplier");
        }else{
            flash("message","addition failed");
          return  redirect("/addsupplier");
        }
           }
              
          } 
   
    public Result eachsale(String id,String name) {
         int idd =Integer.parseInt(id);
         OrderList od= new OrderList(idd,name);
         ArrayList<OrderList> order= shopop.geteachsale(od); 
         return ok(eachsale.render(order,od));
              } 
     public Result eachsalemonthly(String id,String name) {
         int idd =Integer.parseInt(id);
         OrderList od= new OrderList(idd,name);
         String start=session().get("startdate");
         System.out.println("beginning of the month"+start);
         String end=session().get("enddate");
         System.out.println("beginning of the month"+end);
         ArrayList<OrderList> order= shopop.geteachsalemonthly(od,start,end); 
         return ok(eachsale.render(order,od));
              } 
    public Result eachorder(String id,String name) {
         BigInteger idd =new  BigInteger(id);
         System.out.println("order id is >>>>>"+idd); 
         OrderList od= new OrderList(idd,name);
         ArrayList<OrderList> order= shopop.geteachorder(od); 
        return ok(eachorder.render(order,od));
                     }     
        public Result eachordermonthly(String id,String name) {
         BigInteger idd =new  BigInteger(id);
         System.out.println("order id is >>>>>"+idd); 
         OrderList od= new OrderList(idd,name);
         String start=session().get("startdate");
         System.out.println("beginning of the month"+start);
         String end=session().get("enddate");
         System.out.println("beginning of the month"+end);
         ArrayList<OrderList> order= shopop.geteachordermonthly(od,start,end); 
        return ok(eachorder.render(order,od));
                     }       
    public Result order(String id) {
         BigInteger idd =new  BigInteger(id);
          System.out.println("order id is >>>>>"+idd); 
         OrderList od= new OrderList(idd);
         ArrayList<OrderList> order= shopop.geteachorder(od); 
         return ok(eachorder.render(order,od));
                                    }  
     public Result ordermonthly(String id) {
         BigInteger idd =new  BigInteger(id);
          System.out.println("order id is >>>>>"+idd); 
         OrderList od= new OrderList(idd);
         String start=session().get("startdate");
         System.out.println("beginning of the month"+start);
         String end=session().get("enddate");
         ArrayList<OrderList> order= shopop.geteachordermonthly(od,start,end); 
         return ok(eachorder.render(order,od));
                                    }                                                                       
    public Result managerr() {
       String x = session().get("userid");
       System.out.println(x);
        Todaysale today = new Todaysale();
        ArrayList<Todaysale> todaysale=shopop.getonesale(today,x);
      return ok(managerr.render(todaysale,today));
            } 
     public Result resetpassword() {
    if(request().method().equals("GET")){
        return ok(resetpassword.render());
    }else{
        DynamicForm requestData = fmm.form().bindFromRequest();
        Form<ResetPassword> logform = fmm.form(ResetPassword.class);
        ResetPassword rs =logform.bindFromRequest().get();
        shopop.resetpassword(rs);
    }
            return ok(resetpassword.render());
  }  
    public Result selling(String code,String name,String sellingprice){
        System.out.println("method>>>>>"+request().method());
       double sellingp= Double.parseDouble(sellingprice);
        Users us = new Users(Integer.parseInt(session("userid")));
        int quantity=0;
        int q=0;
        long y=0;
        String currentses =session().get("selltime");
        //ArrayList<Products> prod = new ArrayList<Products>();
        ArrayList<Products> prod=shopop.orderedproducts(currentses); 
        Products pr = new Products(code,name,sellingp,quantity);
        DynamicForm requestDatad = fmm.form().bindFromRequest();
        String quant= requestDatad.get("quantity");
             try{
             q =Integer.parseInt(quant);
        }catch(NumberFormatException e){
        } 
               
        if(request().method().equals("GET")){
            return ok(selling.render(prod,us,pr)); 
            
        }else{
            //check   if  anothe r session  is  on       
           System.out.println("method>>>>>"+request().method());           
           String selltime ="";
           String currentsession =session().get("selltime");           
           System.out.println("...order key from session."+currentsession);
           boolean ongoingorder= false;
            if(currentsession !=null && currentsession !="") {
//session  exixts
             System.out.println("onging session"+currentsession);
             ongoingorder=true;
             y=Long.parseLong((session().get("selltime")));
            pr.setTyme(y);
            long settyme = pr.getTyme();
            DynamicForm requestData = fmm.form().bindFromRequest();
             String selling= requestData.get("sellingprice");
             double sp =Double.parseDouble(selling);
             String quanty= requestData.get("quantity");
             quantity =Integer.parseInt(quanty);
            Products prodd = new Products(requestData.get("productcode"),requestData.get("name"),sp,quantity);
            //add  product   to  order list   table with  session  key as  foregn  key  
            System.out.println(">>>>>>>>>>>>>via get is"+q);           
          prod=  shopop.sellProduct(us,prodd,y,ongoingorder,q);       
        }else{
           ongoingorder=false;
            String tyme =Long.toString(System.currentTimeMillis());
            session("selltime",tyme);
        String use =tyme;
        System.out.println("ohhhh new current session....."+use);
        session().get("selltime");
        shopop.sellProduct(us,pr,Long.parseLong(use),ongoingorder,q);
        //create order  in ordr table
        //save  sessionk  is  session
    }
    y=Long.parseLong((session().get("selltime")));
  double totalcost=  shopop.totalSales(y,pr,quantity);
         //out of stock
         Stock stock = new Stock();
         int outofstock=shopop.outofStock(stock);
         if(outofstock <=0){
            flash("outofstock","order out of stock!!");
           // return redirect("/selling");
         }
        
         return redirect("/allproducts");
}

}
public Result neworder(){
    //String use =session().get("selltime");
   long ordno=Long.parseLong((session().get("selltime")));
    shopop.ordercpmpleted(ordno);
   session().remove("selltime");
   System.out.println("session deleted....."+ordno);
     return redirect("/allproducts");
}
public Result addstock() {
    ArrayList<Stock> stok =shopop.allStock();
    if(request().method().equals("GET")){
      return ok(addstock.render(stok)); 
        } else{
           DynamicForm requestData = fmm.form().bindFromRequest();
            String stockid= requestData.get("stockid");
            String productcode= requestData.get("productcode");
            String amount= requestData.get("amountinstock");
            String status = requestData.get("status");
            Stock stock = new Stock(stockid,productcode,amount,status);
            boolean ans=shopop.addStock(stock);
       if(ans == true){

        flash("message","stock added");
        return  redirect("/addstock");
    }else{
        flash("message","addition failed");
      return  redirect("/addstock");
    }
          }     
    
}
public Result allstock() {
    ArrayList<Stock> stok =shopop.allStock();
    ArrayList<Stock> stock= shopop.alloutofStock();
    return ok(allstock.render(stok,stock));
}
public Result editstock(String stockid,String productcode,String amountinstock) {
    Stock stoc = new Stock(stockid,productcode,amountinstock);
    ArrayList<Stock> stok =shopop.allStock();
    if(request().method().equals("GET")){
        return ok(editstock.render(stoc,stok));
    }else{
        DynamicForm requestData = fmm.form().bindFromRequest(); 
        String amountt= requestData.get("amountinstock");
        int amount=Integer.parseInt(amountt);
        String productcod= requestData.get("productcode");
        System.out.println("productcode from the form= "+productcod);
        Stock stock = new Stock(requestData.get("stockid"),productcod,amountt); 
        shopop.editstock(stock,amount,productcod);
    }
    return ok(editstock.render(stoc,stok));``````````
}`1``````````````````````````````````````````````````````````````````````````````````````````````````````````
public Result deletestock(String stockid,String productcode,String amountinstock) {
    Stock stoc = new Stock(stockid,productcode,amountinstock);
    ArrayList<Stock> stok =shopop.allStock();
    shopop.deletestock(stoc);
    
    return redirect("/addstock");
}
public Result salesreport() {
    String srt="";
    String pattern = "yyyy-MM-dd";
     SimpleDateFormat simpleDate = new SimpleDateFormat(pattern);
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.DATE, 1);
    //String srt="2018-10-1";
    srt= simpleDate.format(cal.getTime());
    System.out.println("the first date is :" + cal.getTime()); 
    cal.add(Calendar.MONTH,1);
      System.out.println("next month is: " + cal.getTime());
      //cal.set(Calendar.DATE, 0);
    // String end="2018-10-31";
      String end=simpleDate.format(cal.getTime());
      System.out.println("The last day ofthis month :" + cal.getTime()); 
    Daysale day = new Daysale();
    Todaysale today = new Todaysale();
        Stock stock = new Stock();
        Users us = new Users();
        Products product = new Products();
        ArrayList<OrderList> order = new ArrayList<OrderList>();
        ArrayList<Users> user = shopop.allsellermonthly(srt,end);
        ArrayList<Daysale> todaysale=shopop.monthlysale(day,srt,end);
       int todayscount=shopop.monthlysalecount(today,srt,end);
       shopop.outofStock(stock);
       int x=shopop.allproductcount();
       product.setCount(x);
       double profit=shopop.getmonthlyProfit(srt,end);
       day.setProfit(profit);
      double cost= shopop. getmonthlyCost(srt,end);
      day.setCost(cost);
      if(request().method().equals("GET")){
        System.out.println("method>>>>>"+request().method()); 
    return ok(salesreport.render(todaysale,day,today,user,stock,product,us,order));
}
else{
    DynamicForm requestData = fmm.form().bindFromRequest();
    String month= requestData.get("role");
    int mt=Integer.parseInt(month);
    String year= requestData.get("year");
    int yr=Integer.parseInt(year);
    Monthlysale mon= new Monthlysale(month,year);
    System.out.println("the selected month is............#......"+month+"current cal"+cal);
    System.out.println("the selected year is............#......"+year);
    cal.set(Calendar.YEAR,yr);
    cal.set(Calendar.MONTH,mt);
    srt= simpleDate.format(cal.getTime());
     System.out.println("the set date is :" + cal.getTime()); 
   // ArrayList<Users> user = shopop.allsellermonthly(srt,end);
}
return ok(salesreport.render(todaysale,day,today,user,stock,product,us,order));
}
        String srt="";
        String pattern = "yyyy-MM-dd";
         SimpleDateFormat simpleDate = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        //String srt="2018-10-1";
        srt= simpleDate.format(cal.getTime());
        System.out.println("the first date is :" + cal.getTime()); 
        cal.add(Calendar.MONTH,1);
          System.out.println("next month is: " + cal.getTime());
          cal.add(Calendar.DATE, -1); 
        // String end="2018-10-31";
          String end=simpleDate.format(cal.getTime());
          System.out.println("The last day ofthis month :" + cal.getTime()); 
        Daysale day = new Daysale();
        Todaysale today = new Todaysale();
            Stock stock = new Stock();
            Users us = new Users();
            Products product = new Products();
            ArrayList<OrderList> order = new ArrayList<OrderList>();
            ArrayList<Users> user = shopop.allsellermonthly(srt,end);
            ArrayList<Daysale> todaysale=shopop.monthlysale(day,srt,end);
           int todayscount=shopop.monthlysalecount(today,srt,end);
           shopop.outofStock(stock);
           int x=shopop.allproductcount();
           product.setCount(x);
           double profit=shopop.getmonthlyProfit(srt,end);
           day.setProfit(profit);
          double cost= shopop. getmonthlyCost(srt,end);
          day.setCost(cost);
          if(request().method().equals("GET")){
            System.out.println("method>>>>>"+request().method()); 
        return ok(salesreport.render(todaysale,day,today,user,stock,product,us,order));
    } else{
        Calendar call = Calendar.getInstance();
        DynamicForm requestData = fmm.form().bindFromRequest();
        String month= requestData.get("month");
        int mt=Integer.parseInt(month);
        String year= requestData.get("year");
        int yr=Integer.parseInt(year);
        Monthlysale mon= new Monthlysale(month,year);
        System.out.println("the selected month is............#......"+month);
        System.out.println("the selected year is............#......"+year);
        call.set(Calendar.DATE, 1); 
        call.set(Calendar.YEAR,yr);
        call.set(Calendar.MONTH,mt);
        String monthstart= simpleDate.format(call.getTime());
        session("startdate",monthstart);
        String s=session().get("startdate");
        System.out.println("thes session >>>>>>..."+s);
        System.out.println("the first date is :" + call.getTime());
        call.add(Calendar.MONTH,1);
        System.out.println("the month that will follow: " + call.getTime());
        call.add(Calendar.DATE, -1); 
        String endmonth=simpleDate.format(call.getTime());
        session("enddate",endmonth);
          System.out.println("The last day ofthat month :" + call.getTime()); 
         System.out.println("the set date is :" + call.getTime()); 
       ArrayList<Users> uses = shopop.allsellermonthly(monthstart,endmonth);
       ArrayList<Daysale> todaysales=shopop.monthlysale(day,monthstart,endmonth);
        int todayscounts=shopop.monthlysalecount(today,monthstart,endmonth);
        double profits=shopop.getmonthlyProfit(monthstart,endmonth);
        double costs= shopop. getmonthlyCost(monthstart,endmonth);
    
    return ok(salesreport.render(todaysales,day,today,uses,stock,product,us,order));
    //return redirect("/salesreportmonthly");
    }
}
   


}

