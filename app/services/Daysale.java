/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author essy
 */
public class Daysale {

    private BigInteger id;
    private Date order_date;
    private int order_seller_id;
    private String name;
    private int sellercount;
    private double total;
    private double cost;
    private double profit;
    private BigInteger ordernumber;
    private String productcode;
    private double sellingprice;
    private int quantity;

    public double getSellingprice() {
        return sellingprice;
    }
    public void setSellingprice(double sellingprice) {
        this.sellingprice = sellingprice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

  
    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public BigInteger getId() {
        return id;
    }
    public void setId(BigInteger id) {
        this.id = id;
    }

    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    public int getOrder_seller_id() {
        return order_seller_id;
    }

    public void setOrder_seller_id(int order_seller_id) {
        this.order_seller_id = order_seller_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public int getSellercount() {
        return sellercount;
    }

    public void setSellercount(int sellercount) {
        this.sellercount = sellercount;
    }
    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
    public BigInteger getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(BigInteger ordernumber) {
        this.ordernumber =ordernumber;
    } 

    public Daysale(BigInteger id, Date order_date, int order_seller_id, String name) {
        this.id = id;
        this.order_date = order_date;
        this.order_seller_id = order_seller_id;
        this.name =name;
    }

    public Daysale(BigInteger id, int order_seller_id,String name) {
        this.id = id;
        this.order_seller_id = order_seller_id;
        this.name =name;
    }
    public Daysale(BigInteger id,String name) {
        this.id = id;
        this.name =name;
    }
    public Daysale(BigInteger id, int order_seller_id) {
        this.id = id;
        this.order_seller_id = order_seller_id;
        
    }
    public Daysale() {
    }
    public Daysale(String productcode, String name, double sellingprice, int quantity, double total) {
        this.productcode = productcode;
        this.name = name;
        this.sellingprice = sellingprice;
        this.quantity = quantity;
        this.total = total;
    }
    
}
