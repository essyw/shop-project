/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Products {
    private String productcode;
    private String name;
    private double buyingprice;
    private double sellingprice;
    private int addedby;
    private String status;
    private int soldby;
    private int quantity;
    private long tyme;
    private double totalcost;
    private double total;
    private int stock;
    private int count;

    public double getStock() {
        return stock;
    }
    public void setStock(int stock) {
        this.stock = stock;
    }
    public double getTotal() {
        return total;
    }
    public double getTotalcost() {
        return totalcost;
    }
    public void setTotalcost(double totalcost) {
        this.totalcost = totalcost;
    }
    public String getStatus() {
        return status;
    }

    public int getSoldby() {
        return soldby;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSoldby(int soldby) {
        this.soldby = soldby;
    }
    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBuyingprice() {
        return buyingprice;
    }

    public void setBuyingprice(double buyingprice) {
        this.buyingprice = buyingprice;
    }

    public double getSellingprice() {
        return sellingprice;
    }

    public void setSellingprice(double sellingprice) {
        this.sellingprice = sellingprice;
    }

    public int getAddedby() {
        return addedby;
    }

    public void setAddedby(int addedby) {
        this.addedby = addedby;
    }
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public long getTyme() {
        return tyme;
    }

    public void setTyme(long tyme) {
        this.tyme = tyme;
    }
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Products(String productcode, String name, double buyingprice, double sellingprice, int addedby) {
        this.productcode = productcode;
        this.name = name;
        this.buyingprice = buyingprice;
        this.sellingprice = sellingprice;
        this.addedby = addedby;
    }
    public Products(String productcode, String name,  double buyingprice, double sellingprice) {
        this.productcode = productcode;
        this.name = name;
        this.buyingprice = buyingprice;
        this.sellingprice = sellingprice;
        
    }
    public Products(String productcode, String name, double sellingprice) {
        this.productcode = productcode;
        this.name = name;
        this.sellingprice = sellingprice;
        
    }
   
  public Products(String productcode,String name) {
        this.productcode = productcode;
        this.name = name;
  }
 
  public Products() {
    
}
  public Products(String productcode, String name, double buyingprice, double sellingprice, int addedby,
  String status, int soldby) {
this.productcode = productcode;
this.name = name;
this.buyingprice = buyingprice;
this.sellingprice = sellingprice;
this.addedby = addedby;
this.status = status;
this.soldby = soldby;
}
public Products(String productcode, String name, double buyingprice, double sellingprice, int addedby,
  String status) {
this.productcode = productcode;
this.name = name;
this.buyingprice = buyingprice;
this.sellingprice = sellingprice;
this.addedby = addedby;
this.status = status;
  }
public Products(String productcode, String name, double sellingprice, int quantity) {
    this.productcode = productcode;
    this.name = name;
    this.sellingprice = sellingprice;
    this.quantity = quantity;
}
public Products(String productcode, String name, double sellingprice, int quantity,double total,double totalcost) {
    this.productcode = productcode;
    this.name = name;
    this.sellingprice = sellingprice;
    this.quantity = quantity;
    this.total = total;
    this.totalcost = totalcost;
}
public Products(String productcode, String name, double sellingprice, int quantity,double total) {
    this.productcode = productcode;
    this.name = name;
    this.sellingprice = sellingprice;
    this.quantity = quantity;
    this.total = total;
}
    
}
