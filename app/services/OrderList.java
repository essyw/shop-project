/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.math.BigInteger;
import java.util.Date;
import java.sql.Timestamp;
/**
 *
 * @author essy
 */
public class OrderList {
    private int order_seller_id;
    private BigInteger id;
    private String productcode;
    private String name;
    private double sellingprice;
    private int quantity;
    private double total;
    private double totalcost;
    private BigInteger ordernumber;
    private Date order_date;
    private Timestamp startof_order;
    private Timestamp endof_order;

    public int getOrder_seller_id() {
        return order_seller_id;
    }

    public void setOrder_seller_id(int order_seller_id) {
        this.order_seller_id = order_seller_id;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id =id;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSellingprice() {
        return sellingprice;
    }

    public void setSellingprice(double sellingprice) {
        this.sellingprice = sellingprice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    public double getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(double totalcost) {
        this.totalcost = totalcost;
    }
    public BigInteger getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(BigInteger ordernumber) {
        this.ordernumber =ordernumber;
    } 
    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }
    public Timestamp getStarof_order() {
        return startof_order;
    }

    public void setStartof_order(Timestamp startof_order) {
        this.startof_order = startof_order;
    }
    public Timestamp getEndof_order() {
        return endof_order;
    }

    public void setEndof_order(Timestamp endof_order) {
        this.endof_order = endof_order;
    }
    public OrderList(int order_seller_id,BigInteger id, String productcode, String name, double sellingprice, int quantity, double total) {
        this.order_seller_id = order_seller_id;
        this.id = id;
        this.productcode = productcode;
        this.name = name;
        this.sellingprice = sellingprice;
        this.quantity = quantity;
        this.total = total;
    }
    public OrderList(BigInteger id, String productcode, String name, double sellingprice, int quantity, double total) {
        this.id = id;
        this.productcode = productcode;
        this.name = name;
        this.sellingprice = sellingprice;
        this.quantity = quantity;
        this.total = total;
    }
    public OrderList(String productcode, String name, double sellingprice, int quantity, double total) {
        this.productcode = productcode;
        this.name = name;
        this.sellingprice = sellingprice;
        this.quantity = quantity;
        this.total = total;
    }
    public OrderList(BigInteger ordernumber,String name){
        this.ordernumber =ordernumber;
        this.name = name;
    }
    public OrderList(BigInteger ordernumber){
        this.ordernumber =ordernumber;
       
    }
    public OrderList(int order_seller_id,String name){
        this.order_seller_id = order_seller_id;
        this.name = name;
    }
    public OrderList(){
    
    }
    public OrderList(BigInteger id,Timestamp startof_order,Timestamp endof_order, double total) {
        this.id = id;
        this.startof_order = startof_order;
         this.endof_order = endof_order;
        this.total = total;
    }
}