/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author essy
 */
public class Todaysale {
  //id,order_date,order_seller_id,order_stat
    private BigInteger id;
    private Date order_date;
    private int order_seller_id;
    private String status;
    private double total;
    private int count;
    private int sellercount;
    private BigInteger ordernumber;
    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    public int getOrder_seller_id() {
        return order_seller_id;
    }

    public void setOrder_seller_id(int order_seller_id) {
        this.order_seller_id = order_seller_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    public int getSellercount() {
        return sellercount;
    }

    public void setSellercount(int sellercount) {
        this.sellercount = sellercount;
    }
    public BigInteger getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(BigInteger ordernumber) {
        this.ordernumber =ordernumber;
    } 

    public Todaysale(BigInteger id, Date order_date, int order_seller_id, String status) {
        this.id = id;
        this.order_date = order_date;
        this.order_seller_id = order_seller_id;
        this.status = status;
    }

    public Todaysale(BigInteger id, int order_seller_id) {
        this.id = id;
        this.order_seller_id = order_seller_id;
    }
    public Todaysale(BigInteger id,double total) {
        this.id = id;
        this.total = total;
    }
    public Todaysale() {
    }
    
}
