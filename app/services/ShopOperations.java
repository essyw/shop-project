/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.inject.Singleton;
import javax.inject.*;
import play.db.*;
import java.util.*; 
import java.text.SimpleDateFormat;
import java.math.BigInteger;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 *
 * @author essy
 */
@Singleton
public class ShopOperations {
    Database db;

    @Inject
  public ShopOperations(Database dbb){
      this.db =dbb;
  }  
  public Login getlogin(Login newuser){
    Connection con = this.db.getConnection();
    String name="";
    Login logy=null;
    System.out.println(">>>>"+name);  
    String pattern = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    try{
        Statement st = con.createStatement();
        String log = "select * from users where password ='"+
        newuser.getPassword()+"'AND username='"+newuser.getUsername()+"'"; 
        ResultSet rs = st.executeQuery(log);
        
        System.out.println(log);
        while(rs.next()){
            int id = rs.getInt("user_id");
            String named = rs.getString("name");
            newuser.setName(named);
            name =newuser.getName();
            String usernamed = rs.getString("username");
            String passwordd = rs.getString("password");
            String roled = rs.getString("role");
               logy= new Login(id,named,usernamed,passwordd,roled);
            if(usernamed.equals(newuser.getUsername()) && passwordd.equals(newuser.getPassword())){
                System.out.println(name);           
            }
            else{
               System.out.println("login failed"); 
            }

        }
        String datte ="update users set logintime ='"+ simpleDateFormat.format(new Date())+"'  where password ='"+newuser.getPassword()+"'&& username='"+newuser.getUsername()+"'";       
        System.out.println(datte);  
        st.execute(datte);
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
       return logy;
}
  public boolean addUser(Users newuser){
      //connection to db
   Connection con = this.db.getConnection();
   boolean x = true;
    try{
        String insert ="insert into users(name,username,password,role)values('"+newuser.getName()+"','"+
                newuser.getUsername()+"','"+newuser.getPassword()+"','"+newuser.getRole()+"')";
        Statement st = con.createStatement();
        st.execute(insert);
        st.close();
        con.close();
    }catch (Exception e){
        x = false;
        e.printStackTrace();
    }
   return x;
  }
  public String editUser(Users newuser){
    Connection con = this.db.getConnection();
    try{
        String update="update users set role='"+newuser.getRole()+"' where user_id='"+
        +newuser.getId()+"' AND username ='"+newuser.getUsername()+"'";
        System.out.println(update);
        Statement st = con.createStatement();
        st.execute(update);
        st.close();
        con.close();
    }catch (Exception e){
        e.printStackTrace();
    }  
   return newuser.getName();
  }
  public String deletUser(Users newuser){
    Connection con = this.db.getConnection();
    try{
        String delete="delete from users where user_id ='"+newuser.getId()+"'";
        System.out.println(delete);
        Statement st = con.createStatement();
        st.execute(delete);
        st.close();
        con.close();
    }catch (Exception e){
        e.printStackTrace();
    }  
   return newuser.getName(); 
    }
public boolean addProduct(Products newproduct){
    int stok=0;
    ArrayList<Stock> stock =new ArrayList<Stock>();
    ArrayList<Products> prod = new ArrayList<Products>();
    boolean x = true;
    Connection con = this.db.getConnection();
    try{
        Statement st = con.createStatement();
        //adding in product table
        String add= "insert into products(productcode,name,buyingprice,sellingprice,addedby)values('"+
                newproduct.getProductcode()+"','"+newproduct.getName()+"','"+newproduct.getBuyingprice()+"','"+
                newproduct.getSellingprice()+"','"+newproduct.getAddedby()+"')";      
        st.execute(add);

    //updating stock after successful sale
    String select="select amountinstock from stock where productcode ='"+newproduct.getProductcode()+"'";
    System.out.println(select);
    ResultSet rs= st.executeQuery(select);
    if(rs.next()){
     stok = rs.getInt("amountinstock");
  }
    int amountinstock=stok+1;
    String update ="update stock set amountinstock =" + amountinstock + " where productcode ='"+newproduct.getProductcode()+"'";
    System.out.println(update);
    st.execute(update); 
        st.close();
        con.close();
    }catch(Exception e){
        x = false;
        e.printStackTrace();
    }
    return x;
} 
public void ordercpmpleted(long orderno){
    Connection con =this.db.getConnection(); 
    String status ="completed";
    String pattern = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
     Date date = new Date();
    try{
        String complete="update orders set order_status='"+status+"', endof_order ='"+simpleDateFormat.format(date)+"'where id='"+orderno+"'";
        System.out.println(complete);
        Statement st = con.createStatement();
        st.execute(complete);
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
}
 public ArrayList  sellProduct(Users newuser,  Products newproduct,long tyme,boolean ongoingorder,int quantit){
    Connection con =this.db.getConnection();
     String status ="sold";
     String pattern = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
     Date date = new Date();
    ArrayList<Products> prod = new ArrayList<Products>();
    double total_sales = 0;
    System.out.println("session to db..TTTT>>>>"+tyme);
     try{
         Statement smt =con.createStatement();
          if(ongoingorder ==false){
         //creating order  in ordr table
            System.out.println("new order about to be created");
            String insert="insert into orders(id,order_date,order_seller_id,order_status,totalsale,startof_order) values('"+tyme+"',now(),'"+newuser.getId()+"','on going','"+newproduct.getSellingprice()*quantit+"','"+simpleDateFormat.format(date)+"')";
            System.out.println(insert);
            smt.execute(insert);
         double bp =newproduct.getBuyingprice();
         System.out.println("<<<<>>>>>>>>>>"+bp);
          }
          
          double bp=0;
          String cost="select buyingprice from products where productcode ='"+newproduct.getProductcode()+"'";
          ResultSet r= smt.executeQuery(cost);
          if(r.next()){
           bp = r.getInt("buyingprice");
        }
            //adding  product   to  order list   table with  session  key as  foregn  ke
        String list ="insert into orderlist (ordernumber,productcode,name,sellingprice,quantity,total,totalcost) values ('"+tyme+"','"+newproduct.getProductcode()+"','"+newproduct.getName()+"','"+newproduct.getSellingprice()+"','"+quantit+"','"+newproduct.getSellingprice()*quantit+"','"+bp*quantit+"')";
        System.out.println(list);
        smt.execute(list);

        //fetching  all  items  from   order   list  table  with  forenid  sessionkey
        String select = "select productcode,name,sellingprice,quantity,total from orderlist where ordernumber ='"+tyme+"'";
        System.out.println(select);

        ResultSet rs = smt.executeQuery(select);
        while(rs.next()){
        String name = rs.getString("name");
        String productcode= rs.getString("productcode");
        double sellingp =rs.getDouble("sellingprice");
        int quantity = rs.getInt("quantity");
        double total = rs.getDouble("total");
        Products p = new Products(productcode,name,sellingp,quantity,total);
        prod.add(p);    
    }
         smt.close();
        con.close();
     }catch(Exception e){                                                       
         e.printStackTrace();
     }
     return prod;
 }

 public double totalSales(long tyme,Products newproduct,int quantity){
   Connection con = this.db.getConnection();
   double total_sales = 0;
   int stock=0;
   Products p = new Products();
   p.setStock(stock);
   try{
    Statement st = con.createStatement();
    //getting total cost
        String sum="select sum(total)from orderlist where ordernumber='"+tyme+"'"; 
       ResultSet rss= st.executeQuery(sum);
       while(rss.next()){
          double total =rss.getDouble(1);
          total_sales= total_sales+total;   
      }
      //updating orders total
      String ordersupdate= "update orders set totalsale="+total_sales+ "where id='"+tyme+"'";
      st.execute(ordersupdate);
      System.out.println( ordersupdate);         
     //updating orders totalcost

     double buyingprice=0;
     String total="select sum(totalcost)from orderlist where ordernumber='"+tyme+"'"; 
     ResultSet r =st.executeQuery(total);
     while(r.next()){
         double buyingp=r.getDouble(1);
         buyingprice=buyingprice+buyingp;
     }
     String updt="update orders set totalcost="+buyingprice+"where id='"+tyme+"'";
     st.execute(updt);
   System.out.println(updt);

      //updating stock after successful sale
      int stok=0;
      String select="select amountinstock from stock where productcode ='"+newproduct.getProductcode()+"'";
      ResultSet rs= st.executeQuery(select);
      if(rs.next()){
       stok = rs.getInt("amountinstock");
    }
      int amountinstock=stok-quantity;
      String update ="update stock set amountinstock =" + amountinstock + " where productcode ='"+newproduct.getProductcode()+"'";
     // System.out.println(update);
      st.execute(update);
      st.close();
      con.close();
   }catch(Exception e){
       System.out.println(e);
       e.printStackTrace();
   }
    System.out.println(total_sales);
   return total_sales;
}

 public double getProfit(){
     Daysale newday=new Daysale();
      Connection con = this.db.getConnection();
      String pattern = "yyyy-MM-dd";
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      Date date = new Date();
   double profit = 0;
   //profit of the day
   try{
    Statement st = con.createStatement();
      String sum="select sum(totalsale-totalcost)from orders where order_date='"+simpleDateFormat.format(date)+"'"; 
      ResultSet rs= st.executeQuery(sum);
      while(rs.next()){
          double sellingp =rs.getDouble(1);
          profit+=sellingp;
          newday.setProfit(profit);
          double p=newday.getProfit();
          System.out.println("the profit is......."+p);
      }
      st.close();
      con.close();
   }catch(Exception e){ 
       System.out.println(e);
       e.printStackTrace();
   }
   return profit;
 }
//total cost of the day
public double getCost(){
    Daysale newday=new Daysale();
     Connection con = this.db.getConnection();
     String pattern = "yyyy-MM-dd";
     SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
     Date date = new Date();
  double costt = 0;
  //profit of the day
  try{
   Statement st = con.createStatement();
      String cost="select sum(totalcost)from orders where order_date='"+simpleDateFormat.format(date)+"'"; 
      ResultSet rss= st.executeQuery(cost);
      while(rss.next()){
          costt =rss.getDouble(1);
          newday.setCost(costt);
          System.out.println("the cost is..."+costt);
      }
      st.close();
      con.close();
   }catch(Exception e){ 
       System.out.println(e);
       e.printStackTrace();
   }
   return costt;
 }
public ArrayList geteachsale(OrderList newoder){
    ArrayList<OrderList> order = new ArrayList<OrderList>();
      Connection con = this.db.getConnection();
      String pattern = "yyyy-MM-dd";
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      Date date = new Date();
      double totalsale=0;
   try{
    Statement st = con.createStatement();
    String all="select o.id,o.startof_order,o.endof_order,l.total from orders o,orderlist l where order_date='"+
    simpleDateFormat.format(date)+"'AND order_seller_id='"+newoder.getOrder_seller_id()+"' AND o.id=l.ordernumber";
      System.out.println(all);
      ResultSet rs= st.executeQuery(all);
      while(rs.next()){
        long idd=rs.getLong("id");
        BigInteger id =  BigInteger.valueOf(idd); 
        Timestamp starttime = rs.getTimestamp("startof_order");
        Timestamp endtime = rs.getTimestamp("endof_order");
        double total = rs.getDouble("total");
         totalsale=totalsale+total;
        newoder.setTotalcost(totalsale);
        double tsales=newoder.getTotalcost();
       
    OrderList neworder =  new OrderList(id,starttime,endtime,total);  
    order.add(neworder);
     
      }
      st.close();
      con.close();
   }catch(Exception e){
       System.out.println(e);
       e.printStackTrace();
   }
   return order; 
}
public ArrayList geteachsalemonthly(OrderList newoder,String start,String end){
    ArrayList<OrderList> order = new ArrayList<OrderList>();
      Connection con = this.db.getConnection();
      String pattern = "yyyy-MM-dd";
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      Date date = new Date();
      double totalsale=0;
   try{
    Statement st = con.createStatement();
    String all="select o.id,o.startof_order,o.endof_order,l.total from orders o,orderlist l where order_date BETWEEN '"+start+"' AND '"+end+"' AND order_seller_id='"+newoder.getOrder_seller_id()+"' AND o.id=l.ordernumber";
      System.out.println(all);
      ResultSet rs= st.executeQuery(all);
      while(rs.next()){
        long idd=rs.getLong("id");
        BigInteger id =  BigInteger.valueOf(idd); 
        Timestamp starttime = rs.getTimestamp("startof_order");
        Timestamp endtime = rs.getTimestamp("endof_order");
        double total = rs.getDouble("total");
         totalsale=totalsale+total;
        newoder.setTotalcost(totalsale);
        double tsales=newoder.getTotalcost();
       
    OrderList neworder =  new OrderList(id,starttime,endtime,total);  
    order.add(neworder);
     
      }
      st.close();
      con.close();
   }catch(Exception e){
       System.out.println(e);
       e.printStackTrace();
   }
   return order; 
}
public ArrayList geteachorder(OrderList newoder){
    ArrayList<OrderList> order = new ArrayList<OrderList>();
    ArrayList<Daysale> day = new ArrayList<Daysale>();
      Connection con = this.db.getConnection();
      String pattern = "yyyy-MM-dd";
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      Date date = new Date();
      double totalsale=0;
   try{
    Statement st = con.createStatement();
      String all="select l.productcode,l.name,l.sellingprice,l.quantity,l.total from orders o,orderlist l where order_date='"+
      simpleDateFormat.format(date)+"'AND ordernumber='"+newoder.getOrdernumber()+"' AND o.id=l.ordernumber";
      System.out.println(all);
      ResultSet rs= st.executeQuery(all);
      while(rs.next()){
        String productcode = rs.getString("productcode");
        String name = rs.getString("name");
        double sellingp =rs.getDouble("sellingprice");
        int quantity = rs.getInt("quantity");
        double total = rs.getDouble("total");
        totalsale=totalsale+total;
        newoder.setTotalcost(totalsale);
        double tsales=newoder.getTotalcost();
        System.out.println(tsales);
    OrderList neworder =  new OrderList(productcode,name,sellingp,quantity,total);  
    //Daysale d= new Daysale(productcode,name,sellingp,quantity,total);
    order.add(neworder);
     //day.add(d);
      }
      st.close();
      con.close();
   }catch(Exception e){
       System.out.println(e);
       e.printStackTrace();
   }
   return order; 
}
public ArrayList geteachordermonthly(OrderList newoder,String start,String end){
    ArrayList<OrderList> order = new ArrayList<OrderList>();
    ArrayList<Daysale> day = new ArrayList<Daysale>();
      Connection con = this.db.getConnection();
      String pattern = "yyyy-MM-dd";
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      Date date = new Date();
      double totalsale=0;
   try{
    Statement st = con.createStatement();
      String all="select l.productcode,l.name,l.sellingprice,l.quantity,l.total from orders o,orderlist l where order_date BETWEEN '"+start+"' AND '"+end+"' AND ordernumber='"+newoder.getOrdernumber()+"' AND o.id=l.ordernumber";
      System.out.println(all);
      ResultSet rs= st.executeQuery(all);
      while(rs.next()){
        String productcode = rs.getString("productcode");
        String name = rs.getString("name");
        double sellingp =rs.getDouble("sellingprice");
        int quantity = rs.getInt("quantity");
        double total = rs.getDouble("total");
        totalsale=totalsale+total;
        newoder.setTotalcost(totalsale);
        double tsales=newoder.getTotalcost();
        System.out.println(tsales);
    OrderList neworder =  new OrderList(productcode,name,sellingp,quantity,total);  
    //Daysale d= new Daysale(productcode,name,sellingp,quantity,total);
    order.add(neworder);
     //day.add(d);
      }
      st.close();
      con.close();
   }catch(Exception e){
       System.out.println(e);
       e.printStackTrace();
   }
   return order; 
}
public double geteachprofit(Users newUser){
    Connection con = this.db.getConnection();
   String status ="sold";
   double profit = 0;
   try{
      String sum="select sum(sellingprice-buyingprice)from products where status='"+status+"'AND soldby="+newUser.getId(); 
      Statement st = con.createStatement();
      ResultSet rs= st.executeQuery(sum);
      while(rs.next()){
          double sellingp =rs.getDouble(1);
          profit+=sellingp;
          System.out.println(profit);
      }
      con.close();
   }catch(Exception e){
       System.out.println(e);
       e.printStackTrace();
   }
   return profit; 
}
public ArrayList gettodaysale(Daysale newsales){
    Connection con = this.db.getConnection();
     String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Date date = new Date();
    ArrayList<Daysale> daysale= new ArrayList<Daysale>();
    try{
        Statement st = con.createStatement();
        //sales of the day
        String select ="select o.id,o.order_seller_id,u.name from orders o,users u where o.order_date='"+simpleDateFormat.format(date)+"' AND order_seller_id =user_id group by id"; 
        System.out.println(select);        
        ResultSet rs = st.executeQuery(select);
        while(rs.next()){
            long id=rs.getLong("id"); 
            BigInteger x= BigInteger.valueOf(id);         
            int sellerid=rs.getInt("order_seller_id");
            String name =rs.getString("name");
            Daysale today = new Daysale(x,sellerid,name);
        daysale.add(today);
        }
     
     // total sale value
     double total_daysales=0;
     String sum="select sum(totalsale)from orders where order_date='"+simpleDateFormat.format(date)+"'"; 
     System.out.println(sum);
      ResultSet rss= st.executeQuery(sum);
      while(rss.next()){
          double total =rss.getDouble(1);
         total_daysales= total_daysales+total;
         newsales.setTotal(total_daysales);
         double d =newsales.getTotal();
         System.out.println(d+"...in the method");
      }
      st.close();
      con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return daysale;
}
public ArrayList getonesale(Todaysale newsales,String userid){
    Connection con = this.db.getConnection();
   int usid =Integer.parseInt(userid);
     String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Date date = new Date();
    System.out.println(simpleDateFormat.format(date));
    ArrayList<Todaysale> todaysale= new ArrayList<Todaysale>();
    ArrayList<Daysale> daysale= new ArrayList<Daysale>();
    try{
        Statement st = con.createStatement();
        //sales of the day
       String select ="select id ,totalsale from orders where order_date='"+simpleDateFormat.format(date)+"' AND order_seller_id="+usid; 
       System.out.println(select);        
        ResultSet rs = st.executeQuery(select);
        while(rs.next()){
            long id=rs.getLong("id"); 
            BigInteger x= BigInteger.valueOf(id);
            double total=rs.getDouble("totalsale");
            System.out.println(total);
            Todaysale today = new Todaysale(x,total);
            todaysale.add(today);
        }
        String count ="select count(id)from orders where order_date='"+simpleDateFormat.format(date)+"' AND order_seller_id="+usid; 
       ResultSet rss= st.executeQuery(count);
       while(rss.next()){
     int idd=rss.getInt(1);
     newsales.setSellercount(idd);
         int d =newsales.getCount();
         System.out.println("totalsales count>>>>>>"+idd);
       }
        // total sale value
     double total_daysales=0;
     String sum="select sum(totalsale)from orders where order_date='"+simpleDateFormat.format(date)+"' AND order_seller_id="+usid;  
     System.out.println(sum);
      ResultSet r= st.executeQuery(sum);
      while(r.next()){
          double total =r.getDouble(1);
         total_daysales= total_daysales+total;
         newsales.setTotal(total_daysales);
         double d =newsales.getTotal();
         System.out.println(d+"...in the method");
      }
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return todaysale;
}
public ArrayList allproducts(){
   Products pr = new Products();
    ArrayList<Products> prod = new ArrayList<Products>();
    ArrayList<Products> pro = new ArrayList<Products>();
    Connection con = this.db.getConnection();
    try{
        Statement st= con.createStatement();
        String all ="select * from products ";          
        ResultSet rs = st.executeQuery(all);  
        while(rs.next()){
            String codedb = rs.getString("productcode");
            String namedb = rs.getString("name");
            double buyingp = rs.getDouble("buyingprice");
            double sellingp =rs.getDouble("sellingprice");
            int addedby = rs.getInt("addedby");
            String statusdb = rs.getString("status");
            //int soldby = rs.getInt("soldby");
        Products prd =  new Products(codedb,namedb,buyingp,sellingp,addedby,statusdb);  
        prod.add(prd);     
        }
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return prod;
}
public ArrayList orderedproducts(String currentsession){
    double total_sales = 0;
    ArrayList<Products> prod = new ArrayList<Products>();
    Connection con = this.db.getConnection();
    try{
        //fetching  all  items  from   order   list  table  with  forenid  sessionkey
        String select = "select productcode,name,sellingprice,quantity,total from orderlist where ordernumber ='"+currentsession+"'";
        System.out.println(select);
        Statement state = con.createStatement();
        ResultSet rs = state.executeQuery(select);
        while(rs.next()){
        String name = rs.getString("name");
        String productcode= rs.getString("productcode");
        double sellingp =rs.getDouble("sellingprice");
        int quantity = rs.getInt("quantity");
        double total = rs.getDouble("total");
        Products p = new Products(productcode,name,sellingp,quantity,total);
        prod.add(p); 
        } 
        state.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return prod;
}
public ArrayList allusers(){
 
  ArrayList<Users> user = new ArrayList<Users>();
    Connection con = this.db.getConnection();
    try{
        String all ="select user_id,name,username,role from users ";
      
        Statement st= con.createStatement();
        ResultSet rs = st.executeQuery(all);
  
        while(rs.next()){
            int user_id = rs.getInt("user_id");
            String namedb = rs.getString("name");
            String username = rs.getString("username");
            String role =rs.getString("role");
            
        Users us = new Users(user_id,namedb,username,role);  
        user.add(us);
        
        }
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return user;
}
public ArrayList allsellers(){
    Todaysale today = new Todaysale();
    Users u = new Users();
    ArrayList<Users> user = new ArrayList<Users>();
    String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Date date = new Date();
      Connection con = this.db.getConnection();
      try{
        Statement st= con.createStatement();        
          //count for each seller
          String count="select u.user_id,u.name, count(order_seller_id) from users u,orders o  where order_date='"+simpleDateFormat.format(date)+"'And user_id=order_seller_id group by name";
          System.out.println(count);
         ResultSet rss= st.executeQuery(count);
          while(rss.next()){
            int user_idd= rss.getInt("user_id");
              String named = rss.getString("name"); 
             int eachcount=rss.getInt(3);
                u.setSellercount(eachcount);
                int each=u.getSellercount();      
          Users uss = new Users(user_idd,named,eachcount);  
          user.add(uss); 
         }
         st.close();
          con.close();
      }catch(Exception e){
          e.printStackTrace();
          
      }
      return user;
  }
public String deleteproduct(String code){
    Connection con = this.db.getConnection();
    try{
        String delete="delete from products where productcode='"+code+"'";
        Statement st = con.createStatement();
        System.out.println(delete);
        st.execute(delete);
        st.close();
        con.close();
    }catch(Exception e){
      System.out.println(e);  
    }
    return code;
}
public Products editproduct(Products prod){
    Connection con = this.db.getConnection();
    try{             
        String edit="update products set productcode ='"+prod.getProductcode()+"',name ='"+
        prod.getName()+"', buyingprice= "+prod.getBuyingprice()+", sellingprice = "+prod.getSellingprice()+
        " where productcode = '"+prod.getProductcode()+"'";
        System.out.println(edit);
        Statement st = con.createStatement();
        st.execute(edit);
        st.close();
        con.close();
    }catch(Exception e){
      System.out.println(e);  
    }
    return prod;
}
public boolean addSupplier(Suppliers sup){
     boolean added = true;
    Connection con = this.db.getConnection();
    try{
        String insert ="insert into suppliers(name,description)values('"+sup.getName()+"','"+
                sup.getDescription()+"')";
         System.out.println(insert);
        Statement st = con.createStatement();
        st.execute(insert);
        st.close();
        con.close();
    }catch (Exception e){
        added = false;
        e.printStackTrace();
    }
    return added;
}
public ArrayList allSuppliers() {
 
    ArrayList<Suppliers> supplier = new ArrayList<Suppliers>();
      Connection con = this.db.getConnection();
      try{
          String all ="select id,name,description from suppliers ";
        System.out.println(all);
          Statement st= con.createStatement();
          ResultSet rs = st.executeQuery(all);
          while(rs.next()){
              int id = rs.getInt("id");
              String namedb = rs.getString("name");
              String description = rs.getString("description");             
          Suppliers sup = new Suppliers(namedb,description,id);  
          supplier.add(sup);
                 }
          st.close();
          con.close();
      }catch(Exception e){
          e.printStackTrace();
      }
      return supplier;
  }
public ArrayList monthlysale(Daysale newsales, String frm,String  to){
    Connection con = this.db.getConnection();
    Calendar cal =  Calendar.getInstance();
    String pattern = "yyyy-MM-dd";
   SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
   Date date = new Date();
   ArrayList<Daysale> daysale= new ArrayList<Daysale>();
   try{
       Statement st = con.createStatement();
       //sales of the day
      // String sold ="select * from products where status ='"+status+"' && date BETWEEN '2018-07-20 11:28:33' AND '2018-07-20 11:40:33'";
       String select ="select o.id,o.order_seller_id,u.name from orders o,users u where o.order_date BETWEEN '"+frm+"' AND '"+to+"' AND order_seller_id =user_id group by id"; 
       System.out.println("this months....."+select);        
       ResultSet rs = st.executeQuery(select);
       while(rs.next()){
           long id=rs.getLong("id"); 
           BigInteger x= BigInteger.valueOf(id);         
           int sellerid=rs.getInt("order_seller_id");
           String name =rs.getString("name");
           Daysale today = new Daysale(x,sellerid,name);
       daysale.add(today);
       }
    
    // total sale value
    double total_daysales=0;
    String sum="select sum(totalsale)from orders where order_date BETWEEN '"+frm+"' AND '"+to+"'"; 
    System.out.println(sum);
     ResultSet rss= st.executeQuery(sum);
     while(rss.next()){
         double total =rss.getDouble(1);
        total_daysales= total_daysales+total;
        newsales.setTotal(total_daysales);
        double d =newsales.getTotal();
        System.out.println(d+"...in the method");
     }
     st.close();
     con.close();
   }catch(Exception e){
       e.printStackTrace();
   }
   return daysale;
}
public int monthlysalecount(Todaysale newsales, String frm,String  to){
    
    Connection con = this.db.getConnection();
    String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Date date = new Date();
    int id=0;
    try{
        String count ="select count(id)from orders where order_date BETWEEN '"+frm+"' AND '"+to+"'";
        Statement st =con.createStatement();
       ResultSet rs= st.executeQuery(count);
       while(rs.next()){
     id=rs.getInt(1);
     newsales.setCount(id);
         int d =newsales.getCount();
         System.out.println("totalsales count>>>>>>"+id);
       }
       st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
}
return id;
}
public double getmonthlyProfit( String frm,String  to){
    Daysale newday=new Daysale();
     Connection con = this.db.getConnection();
     String pattern = "yyyy-MM-dd";
     SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
     Date date = new Date();
     double profit = 0;
  //profit of the day
  try{
   Statement st = con.createStatement();
     String sum="select sum(totalsale-totalcost)from orders where order_date BETWEEN '"+frm+"' AND '"+to+"'"; 
     ResultSet rs= st.executeQuery(sum);
     while(rs.next()){
         double sellingp =rs.getDouble(1);
         profit+=sellingp;
         newday.setProfit(profit);
         double p=newday.getProfit();
         System.out.println("the profit is......."+p);
     }
     st.close();
     con.close();
  }catch(Exception e){ 
      System.out.println(e);
      e.printStackTrace();
  }
  return profit;
}
public double getmonthlyCost( String frm,String  to){
    Daysale newday=new Daysale();
     Connection con = this.db.getConnection();
     String pattern = "yyyy-MM-dd";
     SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
     Date date = new Date();
     double costt = 0;
  //profit of the day
  try{
   Statement st = con.createStatement();
      String cost="select sum(totalcost)from orders where order_date BETWEEN '"+frm+"' AND '"+to+"'"; 
      ResultSet rss= st.executeQuery(cost);
      while(rss.next()){
          costt =rss.getDouble(1);
          newday.setCost(costt);
          System.out.println("the cost is..."+costt);
      }
      st.close();
      con.close();
   }catch(Exception e){ 
       System.out.println(e);
       e.printStackTrace();
   }
   return costt;
 }
 public ArrayList allsellermonthly( String frm,String  to){
    Todaysale today = new Todaysale();
    Users u = new Users();
    ArrayList<Users> user = new ArrayList<Users>();
    String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Date date = new Date();
      Connection con = this.db.getConnection();
      try{
        Statement st= con.createStatement();        
          //count for each seller
          String count="select u.user_id,u.name, count(order_seller_id) from users u,orders o  where order_date  BETWEEN '"+frm+"' AND '"+to+"'And user_id=order_seller_id group by name";
          System.out.println(count);
         ResultSet rss= st.executeQuery(count);
          while(rss.next()){
            int user_idd= rss.getInt("user_id");
              String named = rss.getString("name"); 
             int eachcount=rss.getInt(3);
                u.setSellercount(eachcount);
                int each=u.getSellercount();      
          Users uss = new Users(user_idd,named,eachcount);  
          user.add(uss); 
         }
         st.close();
          con.close();
      }catch(Exception e){
          e.printStackTrace();
          
      }
      return user;
  }
public String resetpassword(ResetPassword use){
      Connection con =this.db.getConnection();
      try{
        String reset = "update users set password='"+use.getNewpassword()+"' where username='"+use.getUsername()+
        "' && password='"+use.getOldpassword()+"'";
         System.out.println(reset);
         Statement st = con.createStatement();
         st.execute(reset);
         st.close();
          con.close();
      }catch(Exception e){
        e.printStackTrace(); 
      }

      return use.getNewpassword();
}
public boolean addStock(Stock newstock){
    Connection con = this.db.getConnection();
    boolean x =true;
    try{
        Statement st =con.createStatement();
     String restock="insert into stock(stockid,productcode,amountinstock,status)values('"+
             newstock.getStockid()+"','"+newstock.getProductcode()+"',"+newstock.getAmountinstock()+
             ",'"+newstock.getStatus()+"')";
             System.out.println(restock);        
            st.execute(restock);

      
     st.close();     
     con.close();
    }catch (Exception e){
        x = false;
        e.printStackTrace();
    }
   return x;
     }
 public ArrayList allStock(){
     ArrayList<Stock> stok = new ArrayList<Stock>();
     Connection con = this.db.getConnection();
     try{
         String all ="select * from stock";
         
         Statement st= con.createStatement();
         ResultSet rs = st.executeQuery(all);
         
         while(rs.next()){
             String stockid = rs.getString("stockid");
             String productcode = rs.getString("productcode");
             String amountinstock = rs.getString("amountinstock");
             String status =rs.getString("status");
            Stock stock = new Stock(stockid,productcode,amountinstock,status);
            stok.add(stock);
         }
         st.close();
         con.close();
     }catch(Exception e){
         e.printStackTrace();
     }
     return stok;
 }
 public ArrayList allStockid(){
    ArrayList<Stock> stock = new ArrayList<Stock>();
    Connection con = this.db.getConnection();
    try{
        String selec="select stockid from stock ";
        Statement st= con.createStatement();
        ResultSet r= st.executeQuery(selec);
        System.out.println(selec);
        while(r.next()){
         String stockid = r.getString("stockid");
         Stock stk=new Stock(stockid);
         stock.add(stk);
       //  System.out.println("Size  of  your  arraylist>>"+stock.size()+"The stockid for  first item is >>"+stock.get(0).getStockid());
      }
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return stock;
}
 public Stock editstock(Stock newstock,int amount,String productcode){
    Connection con = this.db.getConnection();
    int stok=0;
    try{
        Statement stt =con.createStatement();
        String select="select amountinstock from stock where productcode ='"+productcode+"'";
        System.out.println(select);
        System.out.println(productcode);
      ResultSet rs= stt.executeQuery(select);
      if(rs.next()){
       stok = rs.getInt("amountinstock");
    }
  int  amountinstock=stok+amount; 
   System.out.println(amountinstock);
  String edit="update stock set amountinstock='"+amountinstock+"' where stockid ='"+newstock.getStockid()+"'"; 
  System.out.println(edit);
    stt.execute(edit);
           //setting status to available when value is >0
    String status = "available";
    String updat ="update stock set status ='"+status+"' where amountinstock >"+0;
    stt.execute(updat);

    //setting status to out of stock when value is 0
     String statu = "outof stock";
     String update ="update stock set status ='"+statu+"' where amountinstock <="+0;
          stt.execute(update);
    stt.close();  
    con.close();   
    }catch(Exception e){
        e.printStackTrace();
    }
  return newstock;  
}
public Stock deletestock(Stock newstock){
    Connection con = this.db.getConnection();
    
    try{
       String delete="delete from stock where stockid='"+newstock.getStockid()+"'"; 
       System.out.println(delete);
       Statement st =con.createStatement();
       st.execute(delete);
    st.close();   
    con.close();   
    }catch(Exception e){
        e.printStackTrace();
    }
  return newstock;  
}
public int todaysalecount(Todaysale newsales){
    
    Connection con = this.db.getConnection();
    String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Date date = new Date();
    int id=0;
    try{
        String count ="select count(id)from orders where order_date='"+simpleDateFormat.format(date)+"'";
        Statement st =con.createStatement();
       ResultSet rs= st.executeQuery(count);
       while(rs.next()){
     id=rs.getInt(1);
     newsales.setCount(id);
         int d =newsales.getCount();
         System.out.println("totalsales count>>>>>>"+id);
       }
       st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
}
return id;
}
public int outofStock(Stock newstock){
    Connection con = this.db.getConnection();
    int amountinstock=0;
    try{
        Statement st =con.createStatement();
     String status = "outof stock";
     String update ="update stock set status ='"+status+"' where amountinstock <="+0;
          st.execute(update); 
          //out of stock count
          String count ="select count(amountinstock)from stock where status='"+status+"'";
          //Statement st =con.createStatement();
          System.out.println(count);
          ResultSet rs= st.executeQuery(count);
          while(rs.next()){
            amountinstock=rs.getInt(1);
            newstock.setCount(amountinstock);
          }
     st.close();
     con.close();
    }catch(Exception e){
         e.printStackTrace(); 
       }
     return amountinstock;
 }
 public ArrayList alloutofStock(){
    ArrayList<Stock> stok = new ArrayList<Stock>();
    Connection con = this.db.getConnection();
    String status ="outof stock";
    try{
        String all ="select stockid, productcode,Status from stock where status='"+status+"'";
        Statement st= con.createStatement();
        ResultSet rs = st.executeQuery(all);
        
        while(rs.next()){
            String stockid = rs.getString("stockid");
            String productcode = rs.getString("productcode");
            String stat =rs.getString("Status");
           Stock stock = new Stock(stockid,productcode,stat);
           stok.add(stock);
        }
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return stok;
}
public int allproductcount(){
    Products pr = new Products();
    Connection con = this.db.getConnection();
    int pcode=0;
    try{
        Statement st =con.createStatement();
        String count ="select count(productcode)from products";
        System.out.println(count);
        ResultSet rss= st.executeQuery(count);
        while(rss.next()){
           pcode=rss.getInt(1);
          pr.setCount(pcode);
        int x=  pr.getCount();
        System.out.println("here all products count is "+x);
        }
     st.close();   
     con.close();
    }catch(Exception e){
         e.printStackTrace(); 
       }
     return pcode;
 }
}


