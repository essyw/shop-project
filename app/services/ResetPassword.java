/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
import play.data.validation.Constraints;
import javax.inject.*;    
public class ResetPassword{
    public String username;
    public String oldpassword;
    public String newpassword;
  // @Required
      public String getUsername() {
          return username;
      }
  
      public void setUsername(String username) {
          this.username = username;
      }
  
      public String getOldpassword() {
          return oldpassword;
      }
  
      public void setNewpassword(String newpassword) {
          this.newpassword = newpassword;
      }
      public String getNewpassword() {
          return newpassword;
      }
  
      public void setOldpassword(String oldpassword) {
          this.oldpassword = oldpassword;
      }
      
  
      public ResetPassword(String username, String oldpassword, String newpassword) {
          this.username = username;
          this.oldpassword = oldpassword;
          this.newpassword = newpassword;
      }
      public ResetPassword(){
  
      }
      
  }
  
      
