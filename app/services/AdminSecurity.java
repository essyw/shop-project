
package services;

import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
//import views.model.LoginTemplate;

public class AdminSecurity extends Security.Authenticator {
    @Override
    public String getUsername(Http.Context ctx) {

     //check username and  roleid

        if (ctx.session().containsKey("username")) {
            return ctx.session().get("username");
        }else {
            return null;
        }
    }

    @Override
    public Result onUnauthorized(Http.Context context) {
        context.flash().put("advice","Not Authorized");
       // LoginTemplate tmplogin = new LoginTemplate("Login");
      return  redirect("/login");

    }

    private String getTokenFromHeader(Http.Context ctx) {
        String[] authTokenHeaderValues = ctx.request().headers().get("X-AUTH-TOKEN");
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            return authTokenHeaderValues[0];
        }else {
            return null;
        }
    }
}