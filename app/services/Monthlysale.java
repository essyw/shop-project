/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Monthlysale {
    String startofmonth;
    String endofmonth;
    String month;
    String year;
    double totalsale;

    public String getStartofmonth() {
        return startofmonth;
    }

    public void setStartofmonth(String startofmonth) {
        this.startofmonth = startofmonth;
    }

    public String getEndofmonth() {
        return endofmonth;
    }

    public void setEndofmonth(String endofmonth) {
        this.endofmonth = endofmonth;
    }

   
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getTotalsale() {
        return totalsale;
    }

    public void setTotalsale(double totalsale) {
        this.totalsale = totalsale;
    }

    public Monthlysale(String startofmonth, String endofmonth, String month, double totalsale) {
        this.startofmonth = startofmonth;
        this.endofmonth = endofmonth;
        this.month = month;
        this.totalsale = totalsale;
    }

    public Monthlysale(String month) {
        this.month = month;
    }
    public Monthlysale(String month,String year) {
        this.month = month;
        this.year = year;
    }
    
}
