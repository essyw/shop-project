/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Suppliers {
   private String name;
   private String description;
   private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Suppliers(String name, String description, int id) {
        this.name = name;
        this.description = description;
        this.id = id;
    }

    public Suppliers(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
   
}
