/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Stock {
    private String stockid;
    private String productcode;
    private String amountinstock;
    private String status;
    private int count;

    public String getStockid() {
        return stockid;
    }

    public void setStockid(String stockid) {
        this.stockid = stockid;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getAmountinstock() {
        return amountinstock;
    }

    public void setAmountinstock(String amountinstock) {
        this.amountinstock = amountinstock;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Stock(String stockid, String productcode, String amountinstock, String status) {
        this.stockid = stockid;
        this.productcode = productcode;
        this.amountinstock = amountinstock;
        this.status = status;
    }
    public Stock(String stockid, String productcode, String amountinstock) {
        this.stockid = stockid;
        this.productcode = productcode;
        this.amountinstock = amountinstock;
        
    }
    public Stock(String stockid) {
        this.stockid = stockid;
        
    }
    public Stock() {
    }
   
    
}
